/**  */

Facebook_Send = React.createClass({
	
	/** Props **/

	propTypes: {
		url: React.PropTypes.string,
		colorscheme: React.PropTypes.oneOf(['light', 'dark']),

		ref: React.PropTypes.string,
	},

	getDefaultProps: function () {
		return {
			url: Meteor.isClient ? window.location.href : '',
			colorscheme: 'dark'
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	componentDidMount: function () {
		deferUntilFBInit(function () {
			FB.XFBML.parse(document);
		});
	},
	componentDidUpdate: function(prevProps, prevState) {
		deferUntilFBInit(function () {
			FB.XFBML.parse(document);
		});
	},

	/** Render **/

	render: function () {
		var url = makeUrl(this.props.url, {
			utm_source: 'facebook',
			utm_medium: 'social'
		});

		var data = {
			'class': "fb-send",
			
			'data-href': url,
			'data-colorscheme': this.props.colorscheme,
			'data-ref': this.props.ref
		}

		var className = 'social-facebook-send ' + (this.props.className ? this.props.className : '');

		/* Construct HTML Injection */

		var props = '';
		_.each(data, function (value, variable) {
			if (!value) return;

			props += ' '+variable+'="'+value+'"';
		});
		var html = '<div'+props+'></div>';

		/* Render */

		return (<div style={this.props.style} className={className} dangerouslySetInnerHTML={{__html: html}} />);
	},
});
