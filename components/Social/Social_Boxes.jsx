/**  */

Social_Boxes = React.createClass({
	
	/** Props **/

	propTypes: {
		url: React.PropTypes.string,

		facebook: React.PropTypes.bool,
		twitter: React.PropTypes.bool,
		linkedin: React.PropTypes.bool,
	},

	getDefaultProps: function () {
		return {
			facebook: true,
			twitter: true,
			linkedin: false // this one we only want to explicitly show when we are doing something which makes sense to share on LinkedIn (ie user profiles etc)
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** Render **/

	render: function () {
		var className = "social-boxes inline-children text-center " + (this.props.className ? this.props.className : "");

		// used to use boxes with vertical count but seems like Twitter removed this functionality so we shifted to a new horizontal layout
		// facebook: layout="box_count"
		// twitter: count="vertical"
		// linkedin: counter="vertical"

		return (
			<div className={className} style={this.props.style}>
				{this.props.facebook ? <Facebook_Like url={this.props.url} layout="button_count" share={true} className="vertical-align-top" /> : null}
				{this.props.twitter ? <Twitter_Tweet url={this.props.url}  /> : null}
				{this.props.linkedin ? <LinkedIn_Share url={this.props.url} /> : null}
			</div>
		);
	},
});
