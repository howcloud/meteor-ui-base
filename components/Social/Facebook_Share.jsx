/**  */

Facebook_Share = React.createClass({
	
	/** Props **/

	propTypes: {
		url: React.PropTypes.string,
		layout: React.PropTypes.oneOf(['button','box_count','button_count','button','icon_link','icon','link']),
	},

	getDefaultProps: function () {
		return {
			url: Meteor.isClient ? window.location.href : '',
			layout: 'button_count',	
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	componentDidMount: function () {
		deferUntilFBInit(function () {
			FB.XFBML.parse(document);
		});
	},
	componentDidUpdate: function(prevProps, prevState) {
		deferUntilFBInit(function () {
			FB.XFBML.parse(document);
		});
	},

	/** Render **/

	render: function () {
		var url = makeUrl(this.props.url, {
			utm_source: 'facebook',
			utm_medium: 'social'
		});

		var data = {
			'class': "fb-share-button",
			
			'data-href': url,
			'data-layout': this.props.layout,
		}

		var className = 'social-facebook-share ' + (this.props.className ? this.props.className : '');

		/* Construct HTML Injection */

		var props = '';
		_.each(data, function (value, variable) {
			if (!value) return;

			props += ' '+variable+'="'+value+'"';
		});
		var html = '<div'+props+'></div>';

		/* Render */

		return (<div style={this.props.style} className={className} dangerouslySetInnerHTML={{__html: html}} />);
	},
});
