/**  */

LinkedIn_Share = React.createClass({
	
	/** Props **/

	propTypes: {
		url: React.PropTypes.string,
		counter: React.PropTypes.oneOf(['none', 'horizontal', 'vertical']),
	},

	getDefaultProps: function () {
		return {
			url: Meteor.isClient ? window.location.href : '',
			counter: 'horizontal',
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	componentDidMount: function () {
		deferUntilLinkedInInit(function () {
			if (IN.parse) IN.parse();
		});
	},
	componentDidUpdate: function(prevProps, prevState) {
		deferUntilLinkedInInit(function () {
			if (IN.parse) IN.parse();
		});
	},

	/** Render **/

	render: function () {
		var url = makeUrl(this.props.url, {
			utm_source: 'linkedin',
			utm_medium: 'social'
		});

		var data = {
			'type': 'IN/Share',
			'data-url': url,
		}

		if (this.props.counter === 'horizontal') {
			data['data-counter'] = 'right';
		} else if (this.props.counter === 'vertical') {
			data['data-counter'] = 'top';
		}

		var className = 'social-linkedin-share ' + (this.props.className ? this.props.className : '');

		var style = {
			maxHeight: this.props.counter === 'vertical' ? 90 : 20,
		}
		if (this.props.style) _.extend(style, this.props.style);

		/* Construct HTML Injection */

		var props = '';
		_.each(data, function (value, variable) {
			if (!value) return;
			
			props += ' '+variable+'="'+value+'"';
		});
		var html = '<script'+props+'></script>';

		/* Render */

		return (<div style={style} className={className} dangerouslySetInnerHTML={{__html: html}} />);
	},
});
