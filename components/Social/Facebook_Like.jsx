/**  */

Facebook_Like = React.createClass({
	
	/** Props **/

	propTypes: {
		url: React.PropTypes.string,
		layout: React.PropTypes.oneOf(['standard', 'box_count', 'button_count', 'button']),
		action: React.PropTypes.oneOf(['like', 'recommend']),
		
		share: React.PropTypes.bool, // whether or not to include a share button
		show_faces: React.PropTypes.bool, // whether or not to show faces of friends who have liked etc
		width: React.PropTypes.number, // width of the plugin, defaults to 0/auto
		height: React.PropTypes.number, 

		ref: React.PropTypes.string, // string up to 50 characters for tracking referrals
	},

	getDefaultProps: function () {
		return {
			url: Meteor.isClient ? window.location.href : '',
			layout: 'button_count',
			action: 'like',
			
			share: false,
			show_faces: false,
			width: 0,
			height: 0,

			ref: '',
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	componentDidMount: function () {
		deferUntilFBInit(function () {
			FB.XFBML.parse(document);
		});
	},
	componentDidUpdate: function(prevProps, prevState) {
		deferUntilFBInit(function () {
			FB.XFBML.parse(document);
		});
	},

	/** Render **/

	render: function () {
		var url = makeUrl(this.props.url, {
			utm_source: 'facebook',
			utm_medium: 'social'
		});

		var data = {
			'class': "fb-like",
			
			'data-href': url,
			'data-layout': this.props.layout,
			'data-action': this.props.action,
			'data-show-faces': this.props.show_faces ? "true" : false,
			'data-share': this.props.share ? "true" : false
		}

		if (this.props.width) data['data-width'] = this.props.width;
		if (this.props.height) data['data-height'] = this.props.height;

		var className = 'social-facebook-like ' + (this.props.className ? this.props.className : '');

		var style = {
			maxHeight: this.props.layout === 'box_count' ? 90 : 20,
		}
		if (this.props.style) _.extend(style, this.props.style);

		/* Construct HTML Injection */

		var props = '';
		_.each(data, function (value, variable) {
			if (!value) return;

			props += ' '+variable+'="'+value+'"';
		});
		var html = '<div'+props+'></div>';

		/* Render */

		return (<div style={style} className={className} dangerouslySetInnerHTML={{__html: html}} />);
	},
});
