/**  */

Twitter_Tweet = React.createClass({
	
	/** Props **/

	propTypes: {
		url: React.PropTypes.string, // url to tweet
		counturl: React.PropTypes.string, // when url to tweet differs from the underlying content being tweeted (ie if we shorten the main url)
		text: React.PropTypes.string, // text to send with the tweet
		via: React.PropTypes.string, // who is this tweet being sent from

		count: React.PropTypes.oneOf(['none', 'horizontal', 'vertical']), // how to position the tweet count box
		size: React.PropTypes.oneOf(['medium', 'large']),

		hashtags: React.PropTypes.string, // tweet a specific hash tag (comma separated)
		mention: React.PropTypes.string, // tweet at a specific user
	},

	getDefaultProps: function () {
		return {
			url: Meteor.isClient ? window.location.href : '',
			text: '',
			via: 'HowCloudOnline', // NOTE: Should be taken from config, config needs to be moved into a package to require it to be loaded before everything else

			count: 'horizontal',
			size: 'medium',
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	componentDidMount: function () {
		this._processUpdate();
	},
	componentDidUpdate: function(prevProps, prevState) {
		this._processUpdate();		
	},

	_processUpdate: function () {
		if (window.twttr && window.twttr.widgets) { // if it is not loaded then we will auto process the whole document when it is loaded anyway
			twttr.widgets.load(ReactDOM.findDOMNode(this));
		}
	},

	/** Render **/

	render: function () {
		var url = makeUrl(this.props.url, {
			utm_source: 'twitter',
			utm_medium: 'social'
		});

		var data = {
			'data-url': url,
			'data-counturl': this.props.counturl,
			'data-via': this.props.via,
			'data-text': this.props.text,
			'data-related': this.props.related,
			'data-count': this.props.count,
			'data-hashtags': this.props.hashtags,
			'data-size': this.props.size
		}

		data.href = 'https://twitter.com/share';

		var child;
		if (this.props.hashtags) {
			data.class = 'twitter-hashtag-button';
			child = '<span>Tweet #'+this.props.hashtags+'</span>';
		} else if (this.props.mention) {
			data.class = 'twitter-mention-button';
			child = '<span>Tweet @'+this.props.mention+'</span>';
		} else {
			data.class = 'twitter-share-button';
			child = '<span>Tweet</span>';
		}

		if (this.props.width) data['data-width'] = this.props.width;

		var className = 'social-twitter-tweet ' + (this.props.className ? this.props.className : '');

		var style = {
			maxHeight: this.props.count === 'vertical' ? 90 : 20,
		}
		if (this.props.style) _.extend(style, this.props.style);

		/* Construct HTML Injection */

		var props = '';
		_.each(data, function (value, variable) {
			if (!value) return;
			
			props += ' '+variable+'="'+value+'"';
		});
		var html = '<a'+props+'>'+child+'</a>';

		/* Render */

		return (<div style={style} className={className} dangerouslySetInnerHTML={{__html: html}} />);
	},
});
