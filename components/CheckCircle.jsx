/**
 * 
 */

CheckCircle = React.createClass({

	/** Props **/

	// propTypes: {
	// todo: allow us to define all sorts here - colors, width/height, solid vs hollow (ie outline) etc etc
	//		 right now this is just what we see in the codepen original (http://codepen.io/haniotis/pen/KwvYLO/) by default
	// },
	// getDefaultProps: function () {
	// 	return {

	// 	}
	// },

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** React **/

	/** Render **/

	render: function () {
		return (
			<svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52" style={this.props.style}>
				<circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
				<path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
			</svg>
		);
	},

});
