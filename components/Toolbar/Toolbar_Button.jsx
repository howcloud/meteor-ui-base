/**  */

Toolbar_Button = React.createClass({

	/** Component Properties **/

	propTypes: {
		disabled: React.PropTypes.bool, // is the button disabled

		onClick: React.PropTypes.func, // onClick function, called even if toggable

		toggle: React.PropTypes.bool, // is the button toggleable?
		defaultToggled: React.PropTypes.bool,
		onToggle: React.PropTypes.func, // onToggle event

		// Might need to todo: toggled: React.PropTypes.bool, // is the button currently toggled? (this is only used if the button is not toggleable but lets us set a toggled looking state anyway)
	},
	getDefaultProps: function () {
		return {
			disabled: false,
			toggle: false,
			defaultToggled: false,
		};
	},

	/** State **/

	getInitialState: function () {
		return {
			toggled: this.props.defaultToggled,
		};
	},

	/** Mixins **/

	mixins: [React.addons.PureRenderMixin],

	/** Toggle **/

	setActive: function () {
		this.setState({toggled: true});
	},
	setInactive: function () {
		this.setState({toggled: false});
	},

	toggle: function () {
		if (!this.props.toggle) return;

		if (this.state.toggled) {
			this.setInactive();
		} else {
			this.setActive();
		}

		if (this.props.onToggle) this.props.onToggle(this);
	},

	/** React **/

	/* Render */

	render: function () {
		return (
			<Toolbar_Elem className={this.props.className} active={this.state.toggled} nopad={this.props.nopad} disabled={this.props.disabled} onClick={this.elem_onClick}>
				{this.props.children}
			</Toolbar_Elem>
		);
	},

	/** UI Events **/

	elem_onClick: function () {
		if (this.props.disabled) return;
		if (this.props.onClick) this.props.onClick(this);
		if (this.props.toggle) this.toggle();
	}

});
