/**  */

/*

Toolbar_Elem
A discrete toolbar element such as a button

*/

Toolbar_Elem = React.createClass({

	/** Component Properties **/

	propTypes: {
		isOwnContainer: React.PropTypes.bool, // does this element contain itself or is there a surrounding container
		active: React.PropTypes.bool,
		nosel: React.PropTypes.bool,
		nopad: React.PropTypes.bool,
		hover: React.PropTypes.bool,
		disabled: React.PropTypes.bool,
		onClick: React.PropTypes.func,
	},

	getDefaultProps: function () {
		return {
			isOwnContainer: true,
			active: false,
			nosel: false,
			nopad: false,
			hover: false,
			disabled: false,
		}
	},
	
	/** Mixins **/

	mixins: [React.addons.PureRenderMixin],

	/* Render */

	render: function () {
		//console.log("RENDER "+this.props.key+" active = "+this.props.active);
		
		var classes = classNames({
			'inline-block': this.props.isOwnContainer, 
			'toolbar-elem-contain': this.props.isOwnContainer,
			'toolbar-elem': true, // Is this container an elem itself?
			'toolbar-elem-def': (!this.props.disabled && !this.props.active && !this.props.nosel), // element in default mode, ie not active
			'toolbar-elem-active': (!this.props.disabled && this.props.active), // element marked as active
			'toolbar-elem-nopad': this.props.nopad, // TODO: no padding to left or right
			'toolbar-elem-nosel': this.props.nosel,
			'toolbar-elem-hover': (!this.props.disabled && this.props.hover), // Force hover style to be applied
			'toolbar-elem-disabled': this.props.disabled
		});

		classes = (this.props.className ? this.props.className+' ' : '') + classes;

		return (
			<div className={classes} onClick={this.props.disabled ? null : this.props.onClick}>
				{this.props.children}
			</div>
		);
	}

});
