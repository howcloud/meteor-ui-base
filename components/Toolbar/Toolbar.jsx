/**  */

/**

Toolbar
Basic wrapper component which sets up the basic toolbar structure to be used by widgets mainly

*/

Toolbar = React.createClass({

	/** Mixins **/

	mixins: [React.addons.PureRenderMixin],

	/** React **/

	/** Render **/

	render: function () {
		return (
			<div className="toolbar">
				<div className="toolbar-inner">
					{this.props.children}
				</div>
			</div>
		);
	}
 
});
