/**  */

/*

HCC_Toolbar_Text
Text to put inside a toolbar element

*/

Toolbar_Text = React.createClass({

	/** Mixins **/

	mixins: [React.addons.PureRenderMixin],

	/* Render */

	render: function () {
		var classes = (this.props.className ? this.props.className+' ' : '')+classNames({
			'inline-block': true,
			'toolbar-elem-text': true,
			'toolbar-elem-text-big': (this.props.big)
		})

		return (
			<div className={classes}>{this.props.children}</div>
		);
	}

});
