/**  */
/*

Toolbar_ElemContainer
Container for a toolbar element when necessary (dropdowns mainly)

*/

var Toolbar_ElemContainer = React.createClass({

	/** Mixins **/

	mixins: [React.addons.PureRenderMixin],

	/* Render */

	render: function () {
		var classes = classNames({
			'inline-block': true, 
			'toolbar-elem-contain': true,
			'toolbar-elem-nopad': this.props.nopad, // TODO: no padding to left or right 
		});

		classes = (this.props.className ? this.props.className+' ' : '') + classes;

		return (
			<div className={classes} onClick={this.props.onClick}>
				{this.props.children}
			</div>
		);
	}

});
