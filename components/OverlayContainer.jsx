/**
 * 
 */
 
OverlayContainer = React.createClass({

	/** Props/State **/

	propTypes: {
		display: React.PropTypes.bool,
		onExitClick: React.PropTypes.func, // if null, does not show an exit icon
	},
	getDefaultProps: function () {
		return {
			display: true,
		};
	},

	getInitialState: function () {
		return {
			top: 0,
			left: 0,
		};
	},

	/** React **/

	shouldComponentUpdate: function (nextProps, nextState) {
		return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
	},

	/** Render **/

	render: function () {
		var overlayClasses = classNames({
			'centered-overlay': true,
			'centered-overlay-display': this.props.display,
			'centered-overlay-exitable': (this.props.onExitClick),
		});

		return (
			<WindowPositionCentre className={overlayClasses}>
				{this.props.onExitClick ? <div className="centered-overlay-exit" onClick={this.props.onExitClick ? this.exit_onClick : null}><FontAwesome icon="times" /></div> : null}
				<div ref="childContainer" className="centered-overlay-childContainer">
					{this.props.children}
				</div>
			</WindowPositionCentre>
		);
	},

	/** UI Events **/

	exit_onClick: function () {
		if (this.props.onExitClick) this.props.onExitClick();
	},

});
