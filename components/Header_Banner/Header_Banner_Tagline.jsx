/**  */

Header_Banner_Tagline = React.createClass({

	/** Props **/

	propTypes: {
		// tagline: could be a string or an array
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		var tagline = this.props.tagline;
		if (!tagline) return null;

		if (tagline) {
			if (!(tagline instanceof Array)) tagline = [tagline];

			tagline = _.map(tagline, function (line, index) {
				return (
					<span key={index} className="line" dangerouslySetInnerHTML={{__html: line}} />
				);
			});
		}


		return (
			<h2 className={this.props.className} style={this.props.style}>
				<span>{tagline}</span>
			</h2>
		);
		
	}
 
});
