/**  */

Header_Banner = React.createClass({

	/** Props **/

	propTypes: {
		backgroundImage: React.PropTypes.string,
		backgroundColor: React.PropTypes.string,

		innerBackgroundColor: React.PropTypes.string,
		innerBackgroundImage: React.PropTypes.string,
		innerStyle: React.PropTypes.object,
		contentAlign: React.PropTypes.oneOf(['default', 'bottom', 'middle']),

		afterHeaderBannerNav: React.PropTypes.bool,
		fixedWidth: React.PropTypes.bool,

		height: React.PropTypes.number,
		minHeight: React.PropTypes.number,
		light: React.PropTypes.bool, // if the background of the Header_Banner is 'light'
		strong: React.PropTypes.bool, // if we want to emphasise the h1 etc more than we normally would
	},
	getDefaultProps: function () {
		return {
			fixedWidth: true,
			contentAlign: 'default', // ie don't apply any auto alignment - we just do 
			// light: false,
		}
	},

	/** State **/

	getInitialState: function () {
		return {

		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Styles **/

		var _height = this.props.height ? this.props.height : 'auto';
		var doAlign = this.props.contentAlign !== 'default' && this.props.minHeight ? true : false;

		/* container */
		// this is actually not used/useful anymore

		var container_className = {
			'header-banner-container': true,
			'header-banner-container-afterNav': this.props.afterHeaderBannerNav
		}

		if (this.props.className) container_className[this.props.className] = true;
		container_className = classNames(container_className);

		/* header-banner */

		var headerBanner_className = classNames({
			'header-banner': true,
			'header-banner-afterHeaderBannerNav': this.props.afterHeaderBannerNav,
			'header-banner-light': this.props.light,
			'header-banner-strong': this.props.strong,

			'inline-children': doAlign
		});

		var headerBanner_style = _.extend({
			backgroundImage: this.props.backgroundImage ? 'url(' + this.props.backgroundImage + ')' : null,
			backgroundColor: this.props.backgroundColor,
			height: _height,
			minHeight: doAlign ? null : this.props.minHeight /* if we are using the align feature then this acts as our method to set a min height */
		}, this.props.innerStyle || {});

		/* inner-container */

		var innerContainer_className = classNames({
			'inner-container': true,
			'fixed-width': this.props.fixedWidth,
		});

		var innerContainer_style = {
			height: _height
		}

		/* inner-container-wrap */

		var innerContainerWrap_className = classNames({
			'inner-container-wrap': true,
			// 'inner-container-wrap-bottom': this.props.contentAlign === 'bottom' ? true : false
		});	

		var innerContainerWrap_style = {
			backgroundColor: this.props.innerBackgroundColor,
			backgroundImage: this.props.innerBackgroundImage ? 'url(' + this.props.innerBackgroundImage + ')' : null
		}

		/* Alignment */

		var contentAlign_style = doAlign ? {
			verticalAlign: this.props.contentAlign
		} : {}

		/** Render **/

		return (
			<div className={container_className} style={this.props.style}>
				<div className={headerBanner_className} style={headerBanner_style}>
					{doAlign ? <div className="aligner" style={_.extend({height: this.props.minHeight}, contentAlign_style)} /> : null}

					<div className={innerContainerWrap_className} style={_.extend(innerContainerWrap_style, contentAlign_style)}>
						<div className={innerContainer_className} style={innerContainer_style}>
							<div>
								{this.props.children}
							</div>
						</div>
					</div>
				</div>
			</div>
		);

	}
 
});
