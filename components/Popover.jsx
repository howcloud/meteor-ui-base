/**
 * 
 */

var _getArrowOffset = function (anchor, myWidth, myHeight) {
	var arrowOffset = 20;

	if (anchor === 1) return {"right": arrowOffset, "left": "auto"};
	if (anchor === 2) return {"top": arrowOffset};
	//if (anchor === 3) return {"top": myHeight/2};
	if (anchor === 4) return {"bottom": arrowOffset, "top": "auto"};
	if (anchor === 5) return {"right": arrowOffset, "left": "auto"};
	//if (anchor === 6) return {"left": myWidth/2};
	if (anchor === 7) return {"left": arrowOffset};
	if (anchor === 8) return {"bottom": arrowOffset, "top": "auto"};
	//if (anchor === 9) return {"top": myHeight/2};
	if (anchor === 10) return {"top": arrowOffset};
	if (anchor === 11) return {"left": arrowOffset};
	//if (anchor === 12) return {"left": myWidth/2};
}

Popover = React.createClass({

	/** Component Properties **/

	propTypes: {
		//selector: React.propTypes.string, // or a dom node
		//anchor: React.PropTypes.string, // or number
		arrowAnchor: React.PropTypes.number, // lets us change where the arrow appears vs where our actual guide is anchored

		title: React.PropTypes.string,
		offset: React.PropTypes.object,

		size: React.PropTypes.oneOf(['auto', 'fluid', 'small']),

		className: React.PropTypes.string,
		style: React.PropTypes.object,

		animated: React.PropTypes.bool, // do we want to apply the class onto the guide popover which animates its movements
		focus: React.PropTypes.bool, // do we want to force the body to focus on this element (ie scroll to it if it is not in view etc)		
		dark: React.PropTypes.bool, // styles to appear on a darker background (vs the light one we assume due to Guide_Highlight)

		_delegateRender: React.PropTypes.bool,
	},
	getDefaultProps: function () {
		return {
			offset: {},
			style: {},

			size: 'auto',
			focus: false,
			dark: true,

			_delegateRender: true,
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
		Position_bySelector_mixin,
	],

	/** Position_bySelector_mixin Optional Overrides **/

	getPositionedNode: function () {
		if (this.props._delegateRender) {
			return this.refs ? this.refs.delegate.getNode() : null;
		} else {
			return this;
		}
	},

	/** Focus **/

	focus: function () {
		if (!this.props.focus) return;

		if (this.state.positionTop) {
			// we focus the top of the page on whichever positioning point is smaller - the actual selector's position or the position of our popover
			var focusTop = this.state.selectorTop < this.state.positionTop ? this.state.selectorTop : this.state.positionTop;
			focusTop -= 20; // pad the top a little
			if (focusTop < 0) focusTop = 0;

			jQuery('html, body').animate({
				scrollTop: focusTop
			}, 750);
		}
	},

	/** React **/

	componentWillMount: function () {
		this.setPositionSelector(this.props.selector, this.props.anchor, true);
	},
	componentWillReceiveProps: function(nextProps) {
		this.setPositionSelector(nextProps.selector, nextProps.anchor);
	},
	componentDidUpdate: function(prevProps, prevState) {
		this.focus();
	},
	componentDidMount: function() {
		this.focus();
	},

	/** Render **/

	render: function () {

		/** Size **/

		var size = this.props.size;
		if (size === 'auto') {
			if (!this.props.title) size = 'small';
		}

		/** Anchor **/

		var anchor = this.props.arrowAnchor || this.props.anchor;
		if (_anchorNameMapping[anchor]) anchor = _anchorNameMapping[anchor];

		/** Popover **/

		var _top;
		var _left;

		if (anchor) {
			if (this.state.positionTop) _top = this.state.positionTop + (this.props.offset.top ? this.props.offset.top : 0);
			if (this.state.positionLeft) _left = this.state.positionLeft + (this.props.offset.left ? this.props.offset.left : 0);

			// if (!_top && !_left) this.couldNotRender = true; // => keep forcing updates until we can (hack hack hack)
			// 												 // the reason for this 
		}

		var popoverStyle = _.extend({
			top: _top,
			left: _left,
			display: 'block',

			// modal mode styling (ie when no anchor)
			position: anchor ? null : 'relative',
			margin: anchor ? null : '60px auto',
			maxWidth: anchor ? null : 420,
		}, this.props.style);

		var popoverClasses = classNames({
			'popover': true,
			
			'popover-dark': this.props.dark,
			'popover-fixed': (this.state.positionType === 'fixed' ? true : false),
			'popover-decorate': this.props.noDecoration ? false : true, // => top blue line
			'popover-animated': this.props.animated ? true : false,

			'popover-small': size === 'small' ? true : false,
			'popover-fluid': size === 'fluid' ? true : false,
		}) + (this.props.className ? ' ' + this.props.className : '');

		/** Arrow **/

		var arrowClasses = anchor ? classNames({
			'popover_arrow': true,
			'popover_arrow_down': (anchor === 11 || anchor === 12 || anchor === 1),
			'popover_arrow_left': (anchor === 2 || anchor === 3 || anchor === 4),
			'popover_arrow_up': (anchor === 5 || anchor === 6 || anchor === 7),
			'popover_arrow_right': (anchor === 8 || anchor === 9 || anchor === 10),
		}) : '';
		
		var arrowStyle = _getArrowOffset(anchor, this.state.thisWidth, this.state.thisHeight);

		/** Render **/

		var popover = (
			<div className={popoverClasses} style={popoverStyle} onClick={this.props.onClick} onMouseEnter={this.props.onMouseEnter} onMouseLeave={this.props.onMouseLeave}>
				<div className="popover_content">
					{this.props.title ? <div className="popover-header">{this.props.title}</div> : null}

					{this.props.children}
				</div>
				{arrowClasses ? <div className={arrowClasses} style={arrowStyle} /> : null}
			</div>
		);

		if (anchor) {
			if (this.props._delegateRender) {
				return (
					<DelegateRender ref="delegate" onMount={this.refreshPosition}>
						{popover}
					</DelegateRender>
				);
			} else {
				return popover;
			}
		} else {
			// if no anchor - we render this as a modal - it's a Guide_Popover which should just be rendered as a general Modal

			return (
				<Modal overlay={false} blur={this.props.blurModal} _delegateRender={this.props._delegateRender}>
					{popover}
				</Modal>
			);
		}
	},

});
