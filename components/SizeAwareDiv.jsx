/**
 * 
 */

/**

SizeAwareDiv
A div which keeps track of and reports back changes in dimensions to a callback function

**/

SizeAwareDiv = React.createClass({

	/** Component Properties **/

	propTypes: {
		onDimensionsChange: React.PropTypes.func,
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
		DimensionsChanged_mixin
	],

	/** Dimensions **/

	getDimensions: function (props, state) {
		var $node = jQuery(ReactDOM.findDOMNode(this));

		return {
			width: $node.outerWidth(),
			height: $node.outerHeight()
		}
	},

	/** React **/

	/** Render **/

	render: function () {
		return (<div {...this.props} />);
	},

});
