/**  */

Splash_Headered = React.createClass({

	/** Props **/

	propTypes: {
		header: React.PropTypes.string,
		light: React.PropTypes.bool,
		strong: React.PropTypes.bool,
		// tagline: React.PropType.string // or .array

		// others all passed through to the Splash
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** React **/

	/** Render **/

	render: function () {
		return (
			<Splash {...this.props} header={null} tagline={null}>
				<Header_Banner light={this.props.light} strong={this.props.strong}>
					<div className="header-banner-header">
						<h1>{this.props.header}</h1>
						{this.props.tagline ? <Header_Banner_Tagline tagline={this.props.tagline} /> : null}
					</div>
				</Header_Banner>

				{this.props.children}
			</Splash>
		);
	},

});
