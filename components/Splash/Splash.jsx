/**  */

Splash = React.createClass({

	/** Props **/

	propTypes: {
		backgroundImage: React.PropTypes.string,
		backgroundPosition: React.PropTypes.string,
		backgroundColor: React.PropTypes.string,
		backgroundRepeat: React.PropTypes.string,

		innerBackgroundImage: React.PropTypes.string,
		innerBackgroundPosition: React.PropTypes.string,
		innerBackgroundColor: React.PropTypes.string,
		innerBackgroundRepeat: React.PropTypes.string,
		innerStyle: React.PropTypes.object,

		main: React.PropTypes.bool, // is this the main (ie first) splash element
		skinny: React.PropTypes.bool,
		innerShadow: React.PropTypes.bool,
		divider: React.PropTypes.bool, // whether or not to show a divider at the bottom

		minHeight: React.PropTypes.number,
		// minWindowHeight: React.PropTypes.bool, // DEPRECATED, now we expect to be passed a minHeight (ie we inherit sizing from MainContainer vs directly listening on the window here)
		adjustHeightForSplashNav: React.PropTypes.bool,
		heightAdjustment: React.PropTypes.number,

		valign: React.PropTypes.oneOf(['top', 'middle', 'bottom']), // allows us to vertically align the content in the middle of the splash (in the event that the splash is > in height than the content)

		onMouseEnter: React.PropTypes.func,
		onMouseLeave: React.PropTypes.func,
		onClick: React.PropTypes.func,
	},
	getDefaultProps: function () {
		return {
			minHeight: 0,
			//minWindowHeight: true,
			adjustHeightForSplashNav: false,
			valign: 'top',
			// divider: false
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			// windowHeight: 0,
			thisHeight: 0
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** Dimensions **/

	// refreshDimensions: function () {
	// 	var $node = this.isMounted() ? jQuery(this.getDOMNode()) : null;

	// 	this.setState({
	// 		windowHeight: jQuery(window).height(),
	// 	});
	// },

	/** React **/

	// componentDidMount: function() {
	// 	this.refreshDimensions();
	// 	window.addEventListener('resize', this.refreshDimensions);
	// },
	// componentWillUnmount: function() {
	// 	window.removeEventListener('resize', this.refreshDimensions);
	// },
	// componentDidUpdate: function(prevProps, prevState) {
	// 	this.refreshDimensions();
	// },

	/** Render **/

	render: function () {

		/** Styles **/

		/* Splash */

		var splash_className = {
			'splash': true,
			'splash-main': this.props.main,
			'splash-skinny': this.props.skinny,
			'splash-divider': this.props.divider,
			'splash-hasBackground': this.props.backgroundImage || this.props.backgroundColor || this.props.innerBackgroundImage || this.props.innerBackgroundColor ? true : false
		}
		if (this.props.className) splash_className[this.props.className] = true;
		splash_className = classNames(splash_className);

		var splash_style = {
			backgroundImage: this.props.backgroundImage ? 'url(' + this.props.backgroundImage + ')' : null,
			backgroundColor: this.props.backgroundColor,
			backgroundPosition: this.props.backgroundPosition,
			backgroundRepeat: this.props.backgroundRepeat
		}
		if (this.props.style) _.extend(splash_style, this.props.style);

		/* Splash Inner */

		var splashInner_minHeight = this.props.minHeight;
		if (splashInner_minHeight) {
			//splashInner_minHeight -= this.props.skinny ? 50 : 100; /* for padding */
			//splashInner_minHeight -= 100;
			splashInner_minHeight -= 60;

			if (this.props.adjustHeightForSplashNav) splashInner_minHeight -= 60;
			// if (this.props.main) splashInner_minHeight -= 20;
			if (this.props.heightAdjustment) splashInner_minHeight += this.props.heightAdjustment;
		}

		var splashInner_style = {
			backgroundImage: this.props.innerBackgroundImage ? 'url('+this.props.innerBackgroundImage+')' : null,
			backgroundPosition: this.props.innerBackgroundPosition,
			backgroundColor: this.props.innerBackgroundColor,
			backgroundRepeat: this.props.innerBackgroundRepeat,

			minHeight: splashInner_minHeight
		}
		if (this.props.innerStyle) _.extend(splashInner_style, this.props.innerStyle);

		var doAlign = this.props.valign !== 'top' && splashInner_minHeight ? true : false;

		var splashInner_className = classNames({
			'splash-inner': true,
			'inline-children': doAlign
		});

		/** Render **/

		return (
			<div className={splash_className} style={splash_style} onMouseEnter={this.props.onMouseEnter} onMouseLeave={this.props.onMouseLeave} onClick={this.props.onClick}>
				<div className={splashInner_className} style={splashInner_style}>
					{doAlign ? <div ref="midaligner" className="aligner" style={{height: splashInner_minHeight, verticalAlign: this.props.valign}} /> : null}

					<div ref="content" className="splash-inner-content" style={{verticalAlign: doAlign ? this.props.valign : null, whiteSpace: 'normal'}}>
						{this.props.children}
						{this.props.innerShadow ? <div className="splash-innerShadow" /> : null}
					</div>
				</div>
			</div>
		);

	}
 
});
