/**  */

SplashTile = React.createClass({

	/** Props **/

	propTypes: {
		backgroundImage: React.PropTypes.string,
		backgroundColor: React.PropTypes.string,

		header: React.PropTypes.string,
		subheader: React.PropTypes.string,

		minHeight: React.PropTypes.number
	},
	getDefaultProps: function () {
		return {
		
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			hover: false,
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** Dimensions **/

	refreshDimensions: function () {
		this.setState({
			windowWidth: jQuery(window).width(),
		});
	},

	/** React **/

	componentDidMount: function() {
		this.refreshDimensions();
		window.addEventListener('resize', this.refreshDimensions);
	},
	componentWillUnmount: function() {
		window.removeEventListener('resize', this.refreshDimensions);
	},
	componentDidUpdate: function(prevProps, prevState) {
		this.refreshDimensions();
	},

	/** Render **/

	render: function () {

		/** Classes & Styles **/

		var isMobile = this.state.windowWidth && this.state.windowWidth < 767 ? true : false;
		var isShowing = this.state.hover || isMobile ? true : false;

		/* Splash */

		var splash_backgroundColor = isShowing ? 'rgba(0,0,0,0.45)' : 'rgba(0,0,0,0.25)'; 
		if (!this.props.backgroundImage) splash_backgroundColor = null; // we only doing the 'tinting' if we have a backgroundImage

		/* Content [children */

		var childrenWrap_className = classNames({
			'show-slide-up force-show-mobile': true,
			'show': isShowing,
			'hidden': !isShowing
		});

		/** Render **/

		return (
			<Splash minHeight={this.props.minHeight} valign="middle" backgroundPosition="center center" backgroundImage={this.props.backgroundImage} backgroundColor={this.props.backgroundColor} innerBackgroundColor={splash_backgroundColor} onMouseEnter={this.splash_mouseEnter} onMouseLeave={this.splash_mouseLeave} style={this.props.style} onClick={this.props.onClick}>
				<Header_Banner>
					<div className="header-banner-header">
						<h1><b>{this.props.header}</b></h1>
						{this.props.subheader ? <h2><b>{this.props.subheader}</b></h2> : null}

						{this.props.children ? <div className={childrenWrap_className}>
							{this.props.children}
						</div> : null}
					</div>
				</Header_Banner>
			</Splash>
		);

	},

	/** UI Callbacks **/

	splash_mouseEnter: function () {
		this.setState({hover: true});
	},
	splash_mouseLeave: function () {
		this.setState({hover: false});
	},
 
});
