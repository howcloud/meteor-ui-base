/**  */

Splash_Nav = React.createClass({

	/** Props **/

	propTypes: {
		backgroundColor: React.PropTypes.string,

		fixed: React.PropTypes.bool, // is this currently fixed to the top of the page?
		// asModule: React.PropTypes.bool, // when this element is fixed should ie be rendered as a module of the header or not (asModule => not the last thing to be rendered in the header)
										   // not yet implemented
		afterHeaderBannerNav: React.PropTypes.bool,

		width: React.PropTypes.number,
		fixedWidth: React.PropTypes.bool,

		fixedStyle: React.PropTypes.object, // a style to only affix when fixed
											// todo: switch to uinge this instead of flags such as afterHeaderBannerNav etc - makes it easier to abstract between sites
	},
	getDefaultProps: function () {
		return {
			fixedWidth: true
		}
	},

	/** State **/

	getInitialState: function () {
		return {

		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Styling **/

		/* splash-nav-container */
		
		var splashNavContainer_className = {
			// (this is probably variable because we had a todo in mind which I can't remember)

			'splash-nav-container': true,
			'splash-nav-container-show': true
		}
		if (this.props.className) splashNavContainer_className[this.props.className] = true;
		splashNavContainer_className = classNames(splashNavContainer_className);

		var splashNavContainer_style = {
			width: this.props.width ? this.props.width : null,
		}
		if (this.props.style) _.extend(splashNavContainer_style, this.props.style);

		/* splash-nav */

		var splashNav_className = classNames({
			'splash-nav': true,
			'splash-nav-fixed': this.props.fixed,
			'splash-nav-fixed-afterHeaderBannerNav': this.props.afterHeaderBannerNav
		});

		var splashNav_style = {
			backgroundColor: this.props.backgroundColor,
			width: this.props.width ? this.props.width : null,
			top: this.props.fixed && this.props.top ? this.props.top : null
		}
		if (this.props.fixedStyle && this.props.fixed) _.extend(splashNav_style, this.props.fixedStyle);

		/* inner-container */

		var innerContainer_className = classNames({
			'inner-container': true,
			'fixed-width': this.props.fixedWidth
		});

		/** Render **/

		return (
			<div className={splashNavContainer_className} style={splashNavContainer_style}>
				<div className={splashNav_className} style={splashNav_style}>
					<div className={innerContainer_className}>
						<div className="clearfix">
							<div>
								{this.props.children}
							</div>
						</div>
					</div>
				</div>
			</div>
		);

	}
 
});
