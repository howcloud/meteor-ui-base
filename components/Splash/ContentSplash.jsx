/**  */

/**

ContentSplash
Used to wrap content we want to display in a splash with the necessary containers to make it fixed width, padded etc

**/

ContentSplash = React.createClass({

	/** Props **/

	propTypes: {
		fixedWidth: React.PropTypes.bool,

		// others all passed through to the Splash
	},
	getDefaultProps: function () {
		return {
			fixedWidth: true
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** React **/

	/** Render **/

	render: function () {
		
		/** Classes & Styles **/

		var innerContainer_className = classNames({
			"inner-container": true,
			"fixed-width": this.props.fixedWidth
		});

		/** Render **/

		return (
			<Splash {...this.props} fixedWidth={null}>
				<div className={innerContainer_className}>
					<div className="content-pad">
						{this.props.children}
					</div>
				</div>
			</Splash>
		);

	},

});
