/**  */

/*

Slider

Offers a single slider interface using jQuery's UI Slider underneath
TODO: Handle multiple sliders as per jQuery feature specification and more events

*/

/** Constants **/

var SLIDER_HORIZONTAL = 'horizontal';
var SLIDER_VERTICAL = 'vertical';

/** Slider **/

Slider = React.createClass({

	/** Component Properties **/

	propTypes: {
		// Slider config
		min: React.PropTypes.number,
		max: React.PropTypes.number,
		orientation: React.PropTypes.oneOf([SLIDER_HORIZONTAL, SLIDER_VERTICAL]),
		step: React.PropTypes.number,
		value: React.PropTypes.number, // if we want to force a particular value
		defaultValue: React.PropTypes.number, // a default value which can be changed away from
		animate: React.PropTypes.bool,

		// Styling of containing div
		className: React.PropTypes.object,
		style: React.PropTypes.object,

		// Callbacks
		onStart: React.PropTypes.func,
		onStop: React.PropTypes.func,
		onChange: React.PropTypes.func, // called when a slider's value change is confirmed (ie drag released)
		onSlide: React.PropTypes.func, // called at every mousemove during slide

		// Render
		display: React.PropTypes.bool,
		disabled: React.PropTypes.bool,
	},

	getDefaultProps: function () {
		return {
			min: 0,
			max: 100,
			defaultValue: 0,

			orientation: SLIDER_HORIZONTAL,
			step: 1,
			animate: false,

			display: true,
			disabled: false,
		}
	},

	/* jQuery Options */

	useDefaultValue: false,

	jQueryOptionsFromProps: function (props) {
		props || (props = this.props);

		return {
			min: props.min,
			max: props.max,
			orientation: props.orientation,
			step: props.step,
			value: (this.useDefaultValue ? props.defaultValue : (typeof props.value === 'undefined' ? this.value() : props.value)),
			animate: props.animate,
			disabled: props.disabled,

			change: this.slider_onChange,
			slide: this.slider_onSlide,
			start: this.slider_onStart,
			stop: this.slider_onStop,
		}
	},

	/** Value **/

	// Value
	// Returns the current value of the slider

	value: function () {
		return jQuery(ReactDOM.findDOMNode(this)).slider('option', 'value');
	},

	/** jquery element **/

	_getJqueryNode: function () {
		if (!this.isMounted()) return null;

		return jQuery(ReactDOM.findDOMNode(this)); // could cache
	},

	/** React **/

	shouldComponentUpdate: function (nextProps, nextState) {
		return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
	},

	/* Lifecycle */

	// componentDidMount
	// Create the jQuery slider after initial render

	componentWillMount: function () {
		if (this.props.defaultValue) this.useDefaultValue = true;
	},
	componentDidMount: function () {
		setTimeout(this._initSlider, 0); // performance booost
	},
	_initSlider: function () {
		var $node = this._getJqueryNode();
		if (!$node) return;

		$node.slider(this.jQueryOptionsFromProps(this.props));
	},

	// componentWillUpdate
	// Manually handle updating the settings of the slider when props have changed

	componentWillUpdate: function (nextProps, nextState) {
		var $slider = this._getJqueryNode();
		if (!$slider) return;

		var options = this.jQueryOptionsFromProps();
		var nextOptions = this.jQueryOptionsFromProps(nextProps);

		_.each(nextOptions, function (value, property) {
			if (value != options[property]) {
				$slider.slider('option', property, value);
			}
		});
	},
	setOption: function (property, value) {
		var $node = this._getJqueryNode();
		if (!$node) return;

		$node.slider('option', property, value);
	},

	/* Render */

	// By just rendering a static html component we know that the React Virtual DOM will never change after its initial render
	// Therefore, we can safely implement jQuery into the lifecycle methods to adjust the actual, underlying DOM

	render: function () {
		var classes = this.props.className || {};
		classes['slider'] = true;
		classes['nodisplay'] = (this.props.display ? false : true);
		var classes = classNames(classes);

		// this.props.children can be used to insert a custom handle, for example

		return (
			<div className={classes} style={this.props.style}>
				{this.props.children}
			</div>
		);
	},

	/** Slider Events **/

	slider_onChange: function (slider) {
		if (this.useDefaultValue) this.useDefaultValue = false; // we have now changed values, so no longer use the default if the default is changed

		if (this.props.onChange) this.props.onChange(this);
	},
	slider_onSlide: function (slider) {
		if (this.props.onSlide) this.props.onSlide(this);
	},
	slider_onStart: function (slider) {
		if (this.props.onStart) this.props.onStart(this);
	},
	slider_onStop: function (slider) {
		if (this.props.onStop) this.props.onStop(this);
		if (this.props.value) this.setOption('value', this.props.value); // if we have a particular value to force
	}

});
