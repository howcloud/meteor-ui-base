/**  */

/**

HeightAlign
Aligns our child again a given height which is rendered invisibly off the side of our container

**/

HeightAlign = React.createClass({

	/** Props **/

	propTypes: {
		height: React.PropTypes.number,
		valign: React.PropTypes.oneOf(['top','middle','bottom'])
	},
	getDefaultProps: function () {
		return {
			height: 0,
			valign: 'middle'
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {
		var vAlignClassName = 'vertical-align-' + this.props.valign;
		var alignerClassName = 'aligner ' + vAlignClassName;

		return (
			<div className="aligner-container">
				<div className={alignerClassName} style={{height: this.props.height}} />

				<div className={vAlignClassName}>
					{this.props.children}
				</div>
			</div>
		);
	}
 
});
