/**
 * 
 */

/**

Equal Blocks
Equally sizes and spaces children apart using equal-blocks system

**/

EqualBlocks = React.createClass({

	/** Component Properties **/

	propTypes: {
		perRow: React.PropTypes.number,

		margin: React.PropTypes.bool,
		widths: React.PropTypes.array, // allows one to override the width of the blocks (ie... they're no longer equal)
		valign: React.PropTypes.oneOf(['top','middle','bottom']),

		containerStyle: React.PropTypes.object, // style to pass to each of the containers around blocks
		containerClassName: React.PropTypes.string,
	},
	getDefaultProps: function () {
		return {
			perRow: 2,
			margin: true,
			valign: 'top',
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Styling & Classes **/

		var className = {
			'equal-blocks': true,
			'equal-blocks-noMargin': !this.props.margin
		}
		className['equal-'+this.props.perRow] = true;
		if (this.props.className) className[this.props.className] = true;
		className = classNames(className);

		/* Container */

		var containerClassName = 'equal-block-container vertical-align-'+this.props.valign+' ' + (this.props.containerClassName ? this.props.containerClassName : '');
		var containerStyle = this.props.containerStyle || {}

		/** Render **/

		var widths = this.props.widths;

		return (
			<div className={className} style={this.props.style}>
				{_.map(this.props.children, function (child, index) {
					var style = {width: widths ? widths[index] : null}
					_.extend(style, containerStyle);

					return (
						<div key={index} className={containerClassName} style={style}>
							<div>
								{child}
							</div>
						</div>
					);
				})}
			</div>
		);

	},

});
