/**  */

Bullets = React.createClass({

	/** Props **/

	propTypes: {
		bullets: React.PropTypes.array,
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		var bullets = this.props.bullets;
		if (!bullets) return null;

		if (bullets) {
			if (!(bullets instanceof Array)) bullets = [bullets];

			bullets = _.map(bullets, function (bullet, index) {
				return (
					<li key={index} dangerouslySetInnerHTML={{__html: bullet}} />
				);
			});
		}

		return (
			<ul className="bullets">
				{bullets}
			</ul>
		);
	}
 
});
