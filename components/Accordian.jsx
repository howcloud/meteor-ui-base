/**  */

Accordian = React.createClass({

	/** Props **/

	propTypes: {
		headers: React.PropTypes.array, // array of headers to use to expand the accordian
										// the accordian content is the actual props.children
	},
	getDefaultProps: function () {
		return {
			headers: []
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			expanded: -1
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Children **/
		// Have to process to remove null elements otherwise they render with blank headers in here or get in the way of working out when the final Accordian_Content is

		var children = [];
		var index = 0;

		React.Children.forEach(this.props.children, function (child) {
			if (child) children.push(React.cloneElement(child, {
				key: index++
			}));
		});

		/** Render **/

		var headers = this.props.headers;
		var expanded = this.state.expanded;

		var content_toggle = this.content_toggle;

		return (
			<div className={"accordian " + (this.props.className ? this.props.className : "")} style={this.props.style}>
				{_.map(children, function (child, index) {
					return (<Accordian_Content key={index} index={index} header={headers[index]} expanded={expanded === index ? true : false} onToggle={content_toggle} final={children.length === index+1 ? true : false}>{child}</Accordian_Content>);
				})}
			</div>
		);

	},

	/** UI Events **/

	content_toggle: function (index) {
		var _expanded = index;
		if (this.state.expanded === _expanded) _expanded = -1;

		this.setState({expanded: _expanded});
	},
 
});

var Accordian_Content = React.createClass({

	/** Props **/

	propTypes: {
		index: React.PropTypes.number, // unique identifier for this isntance of accrdian content, used to toggle it on/off
		header: React.PropTypes.string,
		onToggle: React.PropTypes.func, // when this is toggled
		expanded: React.PropTypes.bool, // whether the content should show
		final: React.PropTypes.bool, // whether this is the final Accordian_Content block in an Accordian
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Styles * Classes */

		var childrenWrap_className = classNames({
			'show-slide-up': true,
			'show': this.props.expanded,
		});

		/** Render **/

		return (
			<div>
				<div onClick={this.header_click} style={{cursor: 'pointer', borderTop: '1px solid rgba(95, 95, 95, 0.25)', borderBottom: this.props.final || this.props.expanded ? '1px solid rgba(95, 95, 95, 0.25)' : null, padding: '10px 15px'}}>
					<div className="inline-block vertical-align-middle" style={{width: '95%'}}>
						<h2 style={{margin: 0, fontSize: 18}}>{this.props.header}</h2>
					</div>

					<div className="inline-block vertical-align-middle text-center" style={{width: '5%'}}>
						<FontAwesome icon={this.props.expanded ? "chevron-up" : "chevron-down"} />
					</div>
				</div>

				<div className={childrenWrap_className}>
					<div style={{borderBottom: this.props.final ? '1px solid rgba(95, 95, 95, 0.25)' : null, padding: '10px 15px', fontSize: 12}}>
						{this.props.children}
					</div>
				</div>
			</div>
		);

	},

	/** UI Events **/

	header_click: function () {
		if (this.props.onToggle) this.props.onToggle(this.props.index);
	},

});
