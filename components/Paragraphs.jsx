/**  */

Paragraphs = React.createClass({

	/** Props **/

	propTypes: {
		// content: could be a string or an array
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		var paragraph = this.props.content;
		if (!paragraph) return null;

		if (paragraph) {
			if (!(paragraph instanceof Array)) paragraph = [paragraph];

			paragraph = _.map(paragraph, function (para, index) {
				return (
					<p key={index} dangerouslySetInnerHTML={{__html: para}} />
				);
			});
		}

		return (
			<div className="paragraphs">
				{paragraph}
			</div>
		);
	}
 
});
