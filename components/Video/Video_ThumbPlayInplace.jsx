/**
 * 
 */

Video_ThumbPlayInplace = React.createClass({

	/** Props **/

	propTypes: {
		youtubeIdentifier: React.PropTypes.string, // youtube identifier for this video
		youtubeListIdentifier: React.PropTypes.string,

		previewUrl: React.PropTypes.string, // preview image url

		defaultPlay: React.PropTypes.bool, // should we start off with this playing
	},
	getDefaultProps: function () {
		return {
			defaultPlay: false,
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** State **/

	getInitialState: function () {
		return {
			play: this.props.defaultPlay
		}
	},

	/** React **/

	/** Render **/

	render: function () {
		if (this.state.play) {
			return (<Video_Player {...this.props} autoplay={true} />);
		} else {
			return (<Video_Thumb {...this.props} onClick={this.video_click} />);
		}
	},

	/** UI Events **/

	video_click: function () {
		this.setState({play: true});
	},

});
