/**
 * 
 */

Video_ThumbLaunchModal = React.createClass({

	/** Props **/

	propTypes: {
		youtubeIdentifier: React.PropTypes.string, // youtube identifier for this video
		youtubeListIdentifier: React.PropTypes.string,

		previewUrl: React.PropTypes.string, // preview image url
	},
	// getDefaultProps: function () {
	// 	return {

	// 	}
	// },

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** React **/

	/** Render **/

	render: function () {
		return (<Video_Thumb {...this.props} onClick={this.class_click} />);
	},

	/** UI Events **/

	class_click: function (teacher) {
		var self = this;

		launchModal({
			props: {
				style: {
					width: 800,
					maxWidth: '90%'
				}
			},

			render: function () {
				return (<Video_Player {...self.props} autoplay={true} />);
			},
		});
	},

});
