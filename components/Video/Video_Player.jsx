/**
 * 
 */

Video_Player = React.createClass({

	/** Props **/

	propTypes: {
		youtubeIdentifier: React.PropTypes.string, // youtube identifier for this video
		youtubeListIdentifier: React.PropTypes.string, // youtube identifier for the video series to play [overrides youtubeIdentifier as the thing to be played]
		
		autoplay: React.PropTypes.bool,
	},
	getDefaultProps: function () {
		return {
			// autoplay: false
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Get SRC **/

		var src;
		if (this.props.youtubeListIdentifier) { // list overrides youtubeIdentifier if provided [might just be providing youtubeIdentifier for the thumbnail]
			src = 'https://www.youtube.com/embed/videoseries?list='+this.props.youtubeListIdentifier;
		} else if (this.props.youtubeIdentifier) {
			src = 'https://www.youtube.com/embed/'+this.props.youtubeIdentifier;
		}

		if (src) {
			src = makeUrl(src, {
				autoplay: this.props.autoplay ? '1' : '0',
				rel: '0',
				vq: 'hd720', // doesn't actually do what we want it to do...
				h: 1, // neither does this
			});
		}

		/** Render **/

		return (
			<div className={["video-wrapper", this.props.className || ""].join(' ')} style={this.props.style}>
				<iframe width={1280} height={720} src={src} frameBorder={0} allowFullScreen={true} />
			</div>
		);

	},

});
