/**
 * 
 */

Video_Thumb = React.createClass({

	/** Props **/

	propTypes: {
		youtubeIdentifier: React.PropTypes.string, // youtube identifier for this video
		youtubeListIdentifier: React.PropTypes.string,

		previewUrl: React.PropTypes.string, // preview image url

		playIcon: React.PropTypes.bool, // should we display the play icon overlayed on the thumb?
		href: React.PropTypes.string, // allows us to render an a tag
	},
	getDefaultProps: function () {
		return {
			playIcon: true,
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Thumb URL **/

		var thumbUrl = this.props.previewUrl;
		if (!thumbUrl) thumbUrl = 'https://img.youtube.com/vi/'+this.props.youtubeIdentifier+'/maxresdefault.jpg';

		/** Render **/

		var containerTag = this.props.href || this.props.onClick ? "a" : "div";

		return React.createElement(containerTag, {
			onClick: this.props.onClick,
			href: this.props.href,

			className: this.props.className,
			style: _.extend({
				display: 'block', 
				position: 'relative'
			}, this.props.style || {})
		},	
			
			(<img src={thumbUrl} style={{width: '100%', display: 'block'}} />),

			(this.props.playIcon ? <span style={{display: 'block', position: 'absolute', top: '50%', left: '50%', marginLeft: -30, marginTop: -30, backgroundColor: 'rgba(0,0,0,0.50)', textAlign: 'center', paddingTop: 10, paddingBottom: 10, fontSize: 25, width: 60, color: '#fff', borderRadius: 5}}>
				<FontAwesome icon="play" />
			</span> : null)

		);

	},

});
