/**  */

/** Require Modules **/

/*

Button
A button component, should handle click events etc
This is now a shorthand for a single sectioned button

*/

Button = React.createClass({

	/** Component Properties **/

	// propTypes: {
	
	// },
	// getDefaultProps: function () {
	// 	return {
			
	// 	}
	// },

	/** Mixins **/

	mixins: [React.addons.pureRenderMixin],

	/** React **/

	/** Render **/

	render: function () {
		return (
			<ButtonShell {...this.props}>
				<span>
					{this.props.children}
				</span>
			</ButtonShell>
		);
	},

});

/*

ButtonShell
ButtonShell is used to just get the button minus any padding => can create multi section buttons etc

*/

ButtonShell = React.createClass({

	/** Component Properties **/

	propTypes: {
		color: React.PropTypes.oneOf(['blue','orange','green','gray','red','black','whiteFrame','blackFrame','greenFrame','redFrame','transparent','shadowFrame']),
		type: React.PropTypes.oneOf(['auto','submit','span']),
		size: React.PropTypes.oneOf(['regular','small','large']),
		flat: React.PropTypes.bool,
		// pad: React.PropTypes.bool,
		inline: React.PropTypes.bool,
		onClick: React.PropTypes.func,
		href: React.PropTypes.string,
		disabled: React.PropTypes.bool,
		pressed: React.PropTypes.bool,
	},
	getDefaultProps: function () {
		return {
			color: 'blue',
			type: 'auto',
			size: 'regular',
			// flat: false,
			// inline: false,
			// pad: false,
			// disabled: false,
		}
	},

	/** Mixins **/

	mixins: [React.addons.pureRenderMixin],

	/** React **/

	/** Render **/

	render: function () {
		var buttonClass_color = 'button-'+this.props.color;
		var buttonClasses = {
			'inline-block': this.props.inline,
			'block': !this.props.inline && this.props.href, // ie only apply this if we have a href here => we'd normally render an inline-block otherwise (ie an a tag)
			'button': true,
			'button-flat': this.props.flat,
			'button-pressed': this.props.pressed,
			'button-disabled': this.props.disabled,
			'button-small': (this.props.size === 'small' ? true : false),
			'button-large': (this.props.size === 'large' ? true : false),
		};
		buttonClasses[buttonClass_color] = true;
		buttonClasses = (this.props.className ? this.props.className + ' ' : '') + classNames(buttonClasses);

		// var renderChildren = this.props.children;
		// if (this.props.pad) renderChildren = (<div class="button-pad">{renderChildren}</div>);

		var props = {
			children: this.props.children,
			className: buttonClasses,
			style: this.props.style,

			onClick: this.button_click,

			onMouseOut: this.props.onMouseOut,
			onMouseEnter: this.props.onMouseEnter,
			onMouseLeave: this.props.onMouseLeave,
			onMouseMove: this.props.onMouseMove,
			onMouseOver: this.props.onMouseOver,

			// ... can populate this with more props as and when we need to (or just switch to a passProps object as a prop on the Button?)
		}

		if (this.props.type === 'submit') { // props.value vs props.children used as value in the case of a submit/button input vs a div/a
			props.type = 'submit';
			props.disabled = this.props.disabled;
			props.value = this.props.value;

			return React.DOM.input(props);
		} else if (this.props.type === 'span') {
			return React.DOM.span(props);
		} else if (this.props.href) { // onClick is not being called here and onMouseDown isn't letting me cancel the action... WTF
			props.href = this.props.href;
			props.target = this.props.target;

			return React.DOM.a(props);
		} else {
			return React.DOM.div(props);
		}
	},

	/** UI Events **/

	button_click: function (sEvent) {
		if (this.props.disabled) return;
		if (this.props.onClick) this.props.onClick(this, sEvent); // NOTE: deprecated to return true/false on a react event handler

		// sEvent.preventDefault();
		// sEvent.stopPropagation();
	},

});
