/**  */

/*

FA = FontAwesome
http://fortawesome.github.io/

TODO: There are a load of possible props we can introduce from http://fortawesome.github.io/Font-Awesome/examples/

*/

FontAwesome = React.createClass({

	/** Component Properties **/

	propTypes: {
		icon: React.PropTypes.string.isRequired,

		flipHorizontal: React.PropTypes.bool,
		flipVertical: React.PropTypes.bool,
		rotate: React.PropTypes.oneOf([0,90,180,270]),
		color: React.PropTypes.string,
		li: React.PropTypes.bool, // is this being rendered as a bullet in an li?

		onClick: React.PropTypes.func,
	},
	getDefaultProps: function () {
		return {
			rotate: 0
		}
	},

	/** React **/

	shouldComponentUpdate: function (nextProps, nextState) {
		return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
	},

	/** Render **/

	render: function () {
		var classes = {
			'fa': true,
			'fa-li': this.props.li,
			'fa-flip-horizontal' : this.props.flipHorizontal,
			'fa-flip-vertical': this.props.flipVertical,
			'fa-rotate-90': this.props.rotate == 90 ? true : false,
			'fa-rotate-180': this.props.rotate == 180 ? true : false,
			'fa-rotate-270': this.props.rotate == 270 ?  true : false
		}

		if (this.props.className) classes[this.props.className] = true;

		var className = 'fa-'+this.props.icon;
		classes[className] = true;
		classes = classNames(classes);

		var style = {color: this.props.color}
		if (this.props.style) _.extend(style, this.props.style);

		return (<i onClick={this.props.onClick} className={classes} style={style}>{this.props.children}</i>);
	},

});
