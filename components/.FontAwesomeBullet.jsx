/**  */

/*

FontAwesomeBullet
Render a bullet point of content with a font awesome icon (of variable size) to the left

*/

FontAwesomeBullet = React.createClass({

	/** Component Properties **/

	propTypes: {
		icon: React.PropTypes.string.isRequired,
		size: React.PropTypes.number, // in px
		fontSize: React.PropTypes.number, // in px - used to midalign first line with bullet
		color: React.PropTypes.string,

		// faStyle: React.PropTypes.object, // todo: lets us apply styles to FontAwesome instance directly
	},
	getDefaultProps: function () {
		return {
			size: 14,
			fontSize: 14
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {

		/** Styles **/

		/* Content alignment */

		var sizeDiff = Math.floor((this.props.size - this.props.fontSize)/2.0); 

		/** Render **/

		return (
			<span className={"block clearfix " + (this.props.className ? this.props.className : "")} style={this.props.style}>
				<FontAwesome icon={this.props.icon} className="block" style={{fontSize: this.props.size, 'float': 'left', color: this.props.color}} />

				<span className="block" style={{'position': 'relative', marginLeft: this.props.size + 7, fontSize: this.props.fontSize, paddingTop: sizeDiff}}>
					{this.props.children}
				</span>
			</span>
		);

	},

});
