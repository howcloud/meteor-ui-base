/**  */

/** 

TextInput
Provides a single line text input (a contenteditable div)
Can optionally expand over multiple lines

TODO: Clean this all up - make it obey the controlled input value paradigm that react input components generally use (ie value vs defaultValue)
	  Right now this is a mess - we don't even do defaultValue correctly....

For reasoning behind how we do onChange processing on the content editable div see: 
http://stackoverflow.com/questions/22677931/react-js-onchange-event-for-contenteditable

**/

/** ** ** ** **/

TextInput = React.createClass({

	/** Component Properties **/

	propTypes: {
		password: React.PropTypes.bool,
		singleLine: React.PropTypes.bool,
		allowHTML: React.PropTypes.bool,
		defaultValue: React.PropTypes.string,
		disabled: React.PropTypes.bool,
		width: React.PropTypes.number,
		maxLenght: React.PropTypes.number,
		error: React.PropTypes.bool,
		size: React.PropTypes.oneOf(['small', 'regular', 'large']),

		placeholder: React.PropTypes.string,
		icon: React.PropTypes.string,

		selectOnFocus: React.PropTypes.bool, // should we select the entire content when we initially focus on this TextInput

		onChange: React.PropTypes.func,
		onFocus: React.PropTypes.func,
		onEnterUp: React.PropTypes.func, // enter key hit event
	},
	getDefaultProps: function () {
		return {
			password: false,
			singleLine: true,
			allowHTML: false,
			disabled: false,
		}
	},

	/** Mixins **/

	mixins: [React.addons.PureRenderMixin],

	/** State **/

	_value: '', // we hold this outside of state because a change in it does not need to trigger a re render, or at least not necessarily
	getInitialState: function () {
		return {
			showPlaceholder: (this.props.placeholder ? (this.props.defaultValue ? false : true) : false),
			defaultValue: this.props.defaultValue // hack: store the default value so that we do not mess around with the input if defaultValue changes after rendering
		};
	},

	/** Focus **/

	isFocus: function () {
		return (document.activeElement === ReactDOM.findDOMNode(this.refs.input));
	},
	focus: function () {
		ReactDOM.findDOMNode(this.refs.input).focus();
	},
	blur: function () {
		ReactDOM.findDOMNode(this.refs.input).blur();
	},

	/** Select **/

	select: function () {
		if (!this.isFocus()) this.focus();

		document.execCommand('selectAll', false, null);
	},

	/** Value **/

	value: function () {
		var _value = this.props.password ? ReactDOM.findDOMNode(this.refs.input).value : ReactDOM.findDOMNode(this.refs.input).innerHTML;
		return this.props.allowHTML || this.props.password ? _value : removeTags(_value);
	},
	setValue: function (_value) {
		if (this.props.password) {
			ReactDOM.findDOMNode(this.refs.input).value = _value;
		} else {
			ReactDOM.findDOMNode(this.refs.input).innerHTML = _value;
		}

		this._value = _value;

		this.refreshPlaceholder();
	},

	/** Placeholder **/

	refreshPlaceholder: function () {
		this._value = this.value(); // setting and getting of this is a bit screwy throughout this component

		if (this.props.placeholder) {
			if (this._value.length) {
				if (this.state.showPlaceholder) {
					this.setState({
						showPlaceholder: false
					});
				}
			} else {
				if (!this.state.showPlaceholder) {
					this.setState({
						showPlaceholder: true
					});
				}
			}
		}
	},

	/** React **/

	componentWillMount: function () {
		this._value = this.props.defaultValue;
	},
	componentDidUpdate: function(prevProps, prevState) {
		this.refreshPlaceholder(); // hacky force checking of whether to show placeholder
	},

	/** Render **/

	render: function () {
		var classes = classNames({
			'input': true,
			'input-error': this.props.error, 
			'textinput': true,
			'textinput-singleLine': this.props.singleLine,
			'textinput-multiline': !this.props.singleLine,
			'textinput-disabled': (this.props.disabled),
			'textinput-icon': (this.props.icon ? true : false),
			'textinput-focused': this.state.focused
		});

		var placeholderClasses = classes + ' ' + classNames({
			// 'input': true,
			'textinput-placeholder': true,
			// 'textinput-singleLine': this.props.singeLine,
			// 'textinput-multiline': !this.props.singleLine,
		});

		var containClasses = {
			'textinput-contain': true,
			'inline-block': this.props.width ? true : false,
			'textinput-large': this.props.size === 'large' ? true : false,
			'textinput-small': this.props.size === 'small' ? true : false
		}
		if (this.props.className) containClasses[this.props.className] = true;
		containClasses = classNames(containClasses);

		var containerStyle = this.props.width ? {width: this.props.width} : {}
		if (this.props.style) _.extend(containerStyle, this.props.style);

		return (
			<div className={containClasses} style={containerStyle}>
				{this.props.password ? 
				  <div className={classes}>
				  	<div>
					  <input ref="input"
					  		 type="password"
					  		 defaultValue={this.state.defaultValue}
					  		 disabled={this.props.disabled}

					  		 onKeyUp={this.input_keyUp}
							 onKeyDown={this.input_keyDown}
							 onKeyPress={this.props.onKeyPress}
							 onDrop={this.input_drop}

							 onFocus={this.input_onFocus}
							 onBlur={this.input_onBlur}

							 onChange={this.props.onChange} />
					</div>
				  </div>
				: <div className={classes}>
					<div>
						<div ref="input"
							 className="input"
							 contentEditable={this.props.disabled ? "false" : "true"}
							 dangerouslySetInnerHTML={{__html: this.state.defaultValue}}

							 onKeyUp={this.input_keyUp}
							 onKeyDown={this.input_keyDown}
							 onKeyPress={this.props.onKeyPress}
							 onDrop={this.input_drop}

							 onFocus={this.input_onFocus}
							 onBlur={this.contentEditable_onBlur}
							 onInput={this.contentEditable_onInput}

							 onPaste={this.onPaste} />
						</div>
					</div>
				}

				{this.props.icon ?
				<div className={placeholderClasses.replace("textinput-icon", "").replace("textinput-focused", "").replace("input-error", "")}>
					<div>
						<div><FontAwesome icon={this.props.icon} /></div>
					</div>
				</div> : null}

				{this.state.showPlaceholder ?
				<div className={placeholderClasses.replace("textinput-focused", "").replace("input-error", "")}>
					<div>
						<div>{this.props.placeholder}</div>
					</div>
				</div> : null}
			</div>
		);
	},

	/** UI Events **/

	input_keyDown: function (sEvent) {
		sEvent.stopPropagation();

		if (this.props.onKeyDown) {
			if (!this.props.onKeyDown(sEvent)) {
				if (sEvent.defaultPrevented) return;
			}
		}
	
		if (this.props.singleLine && sEvent.keyCode == 13) {
			if (this.props.onEnterUp) this.props.onEnterUp(sEvent);
			
			sEvent.preventDefault();
			return;
		}

		if (this.props.maxLength) {
			if (this.value().length >= this.props.maxLength) {
				var c = String.fromCharCode(sEvent.keyCode);
				if (c.match(/\w/)) {
					sEvent.preventDefault();
					return;
				}
			}
		}

		setTimeout(this.refreshPlaceholder, 0);

		return;
	},

	input_keyUp: function (sEvent) {
		this._value = this.value();
		
		//this.refreshPlaceholder();
		if (this.props.onKeyUp) this.props.onKeyUp(sEvent);
	},

	/* Drop */
	// Cancel drops else we can drag images etc in

	input_drop: function (sEvent) {
		sEvent.preventDefault();
		sEvent.stopPropagation();
	},

	/* Focus */
	// Used exclusively when we render an input tag which needs to tell this container that it is focused
	// Having to do this convoluted render and styling could all be gotten around if we just controlled a content editable div to display dots as a value and stored the underlying correct value in a hidden input...

	input_onFocus: function (sEvent) {
		this.setState({focused: true});
		if (this.props.selectOnFocus) setTimeout(this.select, 0);

		if (this.props.onFocus) this.props.onFocus(sEvent);
	},
	input_onBlur: function (sEvent) {
		this.setState({focused: false});
		if (this.props.onBlur) this.props.onBlur(sEvent);
	},

	/* Paste */

	onPaste: function (sEvent) {
		sEvent.preventDefault();

		var text = sEvent.clipboardData ? sEvent.clipboardData.getData('text/plain') : prompt('What do you want to paste?');
		document.execCommand("insertText", false, text);
	},

	contentEditable_onBlur: function (sEvent) {
		if (this.props.onChange) this.props.onChange();
		this.input_onBlur(sEvent);
	},
	contentEditable_onInput: function (sEvent) {
		if (this.props.onChange) this.props.onChange(sEvent);
	},

});
