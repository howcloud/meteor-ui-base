/**
 * 
 */

SuggestInput = React.createClass({

	/** Component Properties **/

	propTypes: {
		placeholder: React.PropTypes.string,
		suggestions: React.PropTypes.array,
		disabled: React.PropTypes.bool,
		width: React.PropTypes.number,
		inline: React.PropTypes.bool,

		onChange: React.PropTypes.func,
		onEnterKey: React.PropTypes.func,
	},
	getDefaultProps: function () {
		return {
			inline: false,
			suggestions: [], // see structure of a suggestion under Value section below
			disabled: false,
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			showSuggestions: true, // this is set based on if we have made a selection
			hideSuggestions: false, // this is set based on focus/blur
		}
	},

	/** Mixins **/

	/** Dimensions **/

	refreshDimensions: function () {
		var $this = jQuery(this.getDOMNode());
		this.setState({width: $this.width()});
	},

	/** Focus **/

	isFocus: function () {
		if (!this.refs.input) return false;
		return this.refs.input.isFocus();
	},
	focus: function () {
		if (!this.refs.input) return;
		return this.refs.input.focus();
	},
	blur: function () {
		if (!this.refs.input) return;
		return this.refs.input.blur();
	},

	/** Value **/

	/*

	value is a suggestion which includes: {
		
		display: string to display
		[value: identifier for this suggestion]
		[anything else relevant to the component passing in suggestions]

	}

	*/

	value: function () {
		if (this._value) return this._value; // the currently selected suggestion

		// todo: if we haven't explicitly set one from the suggestions, search through suggestions and see if what we have typed matches exactly any of the suggestions - pretend that is selected
		
		return null;
	},
	setValue: function (value) {
		this._value = value;
		if (value) this.refs.input.setValue(value.display ? value.display : '');

		this.setState({
			showSuggestions: (value ? false : true),
		});

		if (this.props.onChange) this.props.onChange();
	},
	reset: function () {
		this.setValue(null);
		this.refs.input.setValue('');
		this.setState({showSuggestions: false});
	},

	setInputValue: function (_value) {
		this.refs.input.setValue(_value);
		this.setValue(null);
	},
	inputValue: function () {
		return this.refs.input.value();
	},

	/** React **/

	componentDidMount: function () {
		this.refreshDimensions();
	},
	componentWillReceiveProps: function(nextProps) {
		if (!_.isEqual(nextProps.suggestions, this.props.suggestions)) this.setState({showSuggestions: true});
	},
	shouldComponentUpdate: function (nextProps, nextState) {
		return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
	},

	/** Render **/

	render: function () {
		var suggestion_click = this.suggestion_click;
		var suggestions = this.state.showSuggestions && !this.state.hideSuggestions ? _.map(this.props.suggestions, function (suggestion, index) {
			var click = function () {suggestion_click(suggestion);}	

			return (
				<li key={index} onClick={click} dangerouslySetInnerHTML={suggestion.displayHTML ? {__html: suggestion.displayHTML} : null}>
					{suggestion.displayHTML ? null : <div>{suggestion.display}</div>}
				</li>
			);
		}) : [];

		var className = (this.props.className ? this.props.className+' ' : '') + 'suggestinput' + (this.props.inline ? ' inline-block' : '');

		return (
			<div className={className} style={this.props.style}>
				<TextInput ref="input"  
						   width={this.props.width}
						   placeholder={this.props.placeholder} 
						   icon={this.props.icon}
						   size={this.props.size}
						   
						   onKeyUp={this.textInput_keyUp} 
						   onKeyDown={this.textInput_keyDown} 
						   onFocus={this.textInput_focus} 
						   onBlur={this.textInput_blur} 
						   onEnterKey={this.props.onEnterKey} />

				{suggestions.length ? 
				<ul className="suggestinput-suggestions" style={{width: this.state.width}}>
					{suggestions}
				</ul> : null}
			</div>
		);
	},

	/** UI Events **/

	suggestion_click: function (suggestion) {
		this.setValue(suggestion);
		this.refs.input.focus();
	},
	textInput_keyUp: function (sEvent) {
		if (sEvent.keyCode == 9) {
			if (this.state.showSuggestions && this.props.suggestions.length) {
				this.setValue(this.props.suggestions[0]);
			}
		} else if (sEvent.keyCode == 13) {
			if (this.state.showSuggestions && this.props.suggestions.length) {
				this.setValue(this.props.suggestions[0]);
			} else {
				if (this.props.onEnterKey) this.props.onEnterKey();
			}
		} else {
			this.setValue(null);
		}

		if (this.props.onKeyUp) this.props.onKeyUp(sEvent);
	},
	textInput_keyDown: function (sEvent) {
		if (this.props.onKeyDown) this.props.onKeyDown(sEvent);

		// MOVED TO KEYUP.... why was this in keyDown when enter is in keyUp...?
		// if (sEvent.keyCode == 9) {
		// 	if (this.state.showSuggestions && this.props.suggestions.length) {
		// 		this.setValue(this.props.suggestions[0]);
		// 		return false;
		// 	}
		// }

		return true;
	},

	textInput_focus: function () {
		this.setState({
			hideSuggestions: false
		});
	},
	textInput_blur: function () {
		setTimeout(function () { // terrible hack - but if we hide straight away then we do not get the clicks from one of the suggestions if being clicked
			if (!this.isMounted()) return;
			if (this.refs && this.refs.input && this.refs.input.isFocus()) return;

			this.setState({
				hideSuggestions: true
			});
		}.bind(this), 750);
	},

});
