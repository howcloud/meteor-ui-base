/**  */

/**

TextSelect
Turns regular text into inline selects
Based on https://github.com/javierbyte/react-textselect
Updated to use controlled/uncontrolled component system

**/

TextSelect = React.createClass({

	/** Properties **/

	propTypes: {
		// options: React.PropTypes.object.isRequired, // or an array, basically something enumerable
		// value: React.PropTypes.string.isRequired, // or number if using an array
		onChange: React.PropTypes.func,
	},

	/** State **/

	getInitialState: function () {
		var defaultValue = this.props.value || this.props.defaultValue;
		if (typeof defaultValue === 'undefined') {
			if (this.props.options instanceof Array) {
				defaultValue = 0;
			} else {
				defaultValue = _.keys(this.props.options)[0];
			}
		}

		return {
			value: defaultValue
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** Value **/

	value: function () {
		return this.props.value || this.state.value;
	},

	/** React **/

	/** Render **/

	render: function() {
		var value = this.props.value || this.state.value;

		var classes = 'textselect';
		if (this.props.className) classes += ' ' + this.props.className;

		return (
			<span className={classes} style={this.props.style}>
				{this.props.children ? this.props.children : <span className="textselect-value">{this.props.options[value]}</span>}

				<select className='textselect-input' onChange={this.handleChange} value={value}>
					{_.map(this.props.options, function (display, value) {
						return (
							<option value={value} key={value}>{display}</option>
						);
					})}
				</select>
			</span>
		);
	},

	/** UI Events **/

	handleChange: function(sEvent) {
		var newValue = sEvent.target.value;
		this.setState({value: newValue});

		this.props.onChange(newValue);
	},

});
