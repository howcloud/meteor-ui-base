/**
 * 
 */

/*

PositionInMiddle
Positions child at centre of a width and height

*/

PositionInMiddle = React.createClass({

	/** Props/State **/

	getInitialState: function () {
		return {
			top: 0,
			left: 0,
		};
	},

	/** Mixin **/

	mixins: [React.addons.PureRenderMixin],

	/** Refresh Position **/

	refreshPosition: function () {
		if (!this.isMounted()) return null;

		var $this = jQuery(ReactDOM.findDOMNode(this.refs.childContainer));
		var childWidth = $this.outerWidth();
		var childHeight = $this.outerHeight();

		this.setState({
			top: Math.floor((this.props.height-childHeight)/2.0),
			left: Math.floor((this.props.width-childWidth)/2.0),
		});
	},

	/** React **/

	componentDidUpdate: function(prevProps, prevState) {
		this.refreshPosition();
	},
	componentDidMount: function () {
		this.refreshPosition();
	},

	/** Window Event **/

	windowResized: function () {
		if (this.props.display) this.refreshPosition();
	},

	/** Render **/

	render: function () {
		if (!this.props.width) return null;

		var overlayStyle = {
			position: 'absolute',
			top: this.state.top+'px',
			left: this.state.left+'px'
		};

		return (
			<div className={this.props.className} style={_.extend({position: 'relative', width: this.props.width, height: this.props.height}, this.props.style || {})}>
				<div ref="childContainer" style={overlayStyle}>
					{this.props.children}
				</div>
			</div>
		);
	},

});
