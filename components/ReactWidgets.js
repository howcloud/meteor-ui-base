Meteor.startup(function () {
	setTimeout(function () { // force this to be deferred to the end of other startup functions 
		if (typeof ReactWidgets !== 'undefined') {
			var makeGlobal = typeof window === 'undefined' ?  global : window; // for some reason we have to do this despite exporting the variable at the package level
											   								   // probably because of all the deferring to startup etc

			makeGlobal['DropdownList'] = ReactWidgets.DropdownList;
			makeGlobal['Combobox'] = ReactWidgets.Combobox;
			makeGlobal['NumberPicker'] = ReactWidgets.NumberPicker;
			makeGlobal['Multiselect'] = ReactWidgets.Multiselect;
			makeGlobal['SelectList'] = ReactWidgets.SelectList;
			makeGlobal['Calendar'] = ReactWidgets.Calendar;
			makeGlobal['DateTimePicker'] = ReactWidgets.DateTimePicker;
		}
	}, 0);
});