/**
 * 
 */

/*

ModalContentBox
Opens a content box styled modal using the uderlying Modal component
Props are passed through to Modal

*/

ModalContentBox = React.createClass({

	/** Props/State **/

	propTypes: {
		title: React.PropTypes.string,
		titleIcon: React.PropTypes.string,

		// all props passed through to Modal to allow override of Modal props  
	},
	// getDefaultProps: function () {
	// 	return {
			
	// 	}
	// },

	/** Mixins **/

	mixins: [
		React.addons.pureRenderMixin,
	],

	/** State **/

	getInitialState: function () {
		return {
			
		}
	},

	/** Dimensions **/

	refreshDimensions: function () {
		this.setState({
			windowHeight: jQuery(window).height()
		});
	},

	/** React **/

	componentDidMount: function () {
		this.refreshDimensions();
		window.addEventListener('resize', this.refreshDimensions);
	},
	componentWillUnmount: function () {
		window.removeEventListener('resize', this.refreshDimensions);
	},

	/** Render **/

	render: function () {
		// NOTE: We don't pass style into the actual Modal container just to the modal-content-box itself

		return (
			<Modal {...this.props}>
				<HeightAlign height={this.state.windowHeight}>
					<div className="modal-content-box content-box">
						{this.props.title ? <h1>{this.props.titleIcon ? <FontAwesome icon={this.props.titleIcon} /> : null} {this.props.title}</h1> : null}

						{this.props.children}
					</div>
				</HeightAlign>
			</Modal>
		);
	},

});
