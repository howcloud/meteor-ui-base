/**
 * 
 */

/*

Modal
Open above the site content and (in theory) block interaction with it
Allow us to display messages to the user and prompt them for specific actions

*/

var _openModalCount = 0; // number of open modals
					     // we use this to decide whether or not to remove the open-modal class from the body of the page

Modal = React.createClass({

	/** Props/State **/

	propTypes: {
		onCancel: React.PropTypes.func, // if not set then Modal cannot be cancelled through normal means (click on overlay etc)
		
		overlay: React.PropTypes.bool, // whether or not to render an overlay behind this Modal
									   // we support not showing an overlay in cases where an overlay is being manually managed (ie with a Guider, for example)
		blur: React.PropTypes.bool,

		_delegateRender: React.PropTypes.bool, // whether or not to delegate the rendering of this Modal to the main site controller  
	},
	getDefaultProps: function () {
		return {
			overlay: true,
			blur: true,

			_delegateRender: true
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.pureRenderMixin,
	],

	/** Events **/

	cancel: function () {
		if (this.props.onCancel) this.props.onCancel();
	},

	/** React **/

	componentDidMount: function() {
		if (++_openModalCount === 1) {
			setTimeout(function() { // hack: guarantees HC.SiteController instance is defined
				if (this.props.blur) HC.SiteControllerInstance.blur();
				HC.SiteControllerInstance.freezeBodyScroll();
			}.bind(this), 0);
		}
	},
	componentWillUnmount: function () {
		if (--_openModalCount === 0) {
			if (this.props.blur) HC.SiteControllerInstance.unblur();
			HC.SiteControllerInstance.unfreezeBodyScroll();
		}
	},

	/** Render **/

	render: function () {

		/** Render Modal **/

		var modal = (
			<div>
				{this.props.overlay ? <div className="overlay overlay-cover overlay-backdrop-semiblack" /> : null}

				<div className="overlay overlay-cover overlay-content-container" onClick={this.cancel}>
					<div className={'overlay-content ' + (this.props.className ? this.props.className :  '')} style={this.props.style} onClick={this.modalContent_click}>
						{this.props.children}
					</div>
				</div>
			</div>
		);

		/** Render Delegation **/

		if (this.props._delegateRender) {
			return (
				<DelegateRender>
					{modal}
				</DelegateRender>
			);
		} else {
			return modal;
		}

	},

	/** UI Events **/

	modalContent_click: function (sEvent) {
		sEvent.stopPropagation(); // prevent the overlay click event which cancels the modal
	},

});
