/**  */

/**

RichTextarea_Toolbar

**/

RichTextarea_Toolbar = React.createClass({

	/** Component Properties **/

	propTypes: {
		commandState: React.PropTypes.object, // command: true/false ie if toggled/active
		onCommandToggle: React.PropTypes.func, // callback to clicking on a toolbar button
		disabled: React.PropTypes.bool,
	},
	getDefaultProps: function () {
		return {
			commandState: {},
		}
	},

	/** Command **/

	commandToggle: function (command, value) {
		if (this.props.onCommandToggle) this.props.onCommandToggle(command, value);
	},

	/** React **/

	shouldComponentUpdate: function (nextProps, nextState) {
		//return !util.isEqual(nextProps, this.props) || !util.isEqual(nextState, this.state);
		return !_.isEqual(nextProps.commandState, this.props.commandState); // this is the only thing we need to worry about for this component as it stands
	},

	/** Render **/

	render: function () {
		return (
			<div className="rtextarea-toolbar clearfix-left">
				<div className="rtextarea-toolbar-border" />

				<div className="toolbar-left">
					<Toolbar_Elem onClick={this.bold_toggle} active={this.props.commandState['bold']} nopad={true} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="bold" />
						</Toolbar_Text>
					</Toolbar_Elem>
					<Toolbar_Elem onClick={this.underline_toggle} active={this.props.commandState['underline']} nopad={true} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="underline" />
						</Toolbar_Text>
					</Toolbar_Elem>
					<Toolbar_Elem onClick={this.italic_toggle} active={this.props.commandState['italic']} nopad={true} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="italic" />
						</Toolbar_Text>
					</Toolbar_Elem>
					<Toolbar_Elem onClick={this.strikethrough_toggle} active={this.props.commandState['strikeThrough']} nopad={true} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="strikethrough" />
						</Toolbar_Text>
					</Toolbar_Elem>

					<Toolbar_Elem onClick={this.listOl_toggle} active={this.props.commandState['insertOrderedList']} nopad={true} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="list-ol" />
						</Toolbar_Text>
					</Toolbar_Elem>
					<Toolbar_Elem onClick={this.listUl_toggle} active={this.props.commandState['insertUnorderedList']} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="list-ul" />
						</Toolbar_Text>
					</Toolbar_Elem>
				</div>

				<div className="toolbar-right toolbar-right-force">
					<Toolbar_Elem onClick={this.link_insert} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="link" />
						</Toolbar_Text>
					</Toolbar_Elem>
					<Toolbar_Elem onClick={this.equation_insert} nopad={true} disabled={this.props.disabled}>
						<Toolbar_Text big={true}>Σ</Toolbar_Text>
					</Toolbar_Elem>
					<Toolbar_Elem onClick={this.quote_toggle} active={this.props.commandState['formatBlock']} nopad={true} disabled={this.props.disabled}>
						<Toolbar_Text>
							<FontAwesome icon="quote-right" />
						</Toolbar_Text>
					</Toolbar_Elem>
				</div>
			</div>
		);
	},

	/** UI Events **/

	bold_toggle: function () {
		this.commandToggle('bold');
	},
	underline_toggle: function () {
		this.commandToggle('underline');
	},
	italic_toggle: function () {
		this.commandToggle('italic');
	},
	strikethrough_toggle: function () {
		this.commandToggle('strikethrough');
	},

	listOl_toggle: function () {
		this.commandToggle('insertOrderedList');
	},
	listUl_toggle: function () {
		this.commandToggle('insertUnorderedList');
	},

	equation_insert: function () {
		this.commandToggle('equation');
	},
	quote_toggle: function () {
		// Note from https://developer.mozilla.org/en-US/docs/Web/API/document.execCommand
		// Internet Explorer supports only heading tags H1 - H6, ADDRESS, and PRE, which must also include the tag delimiters < >, such as "<H1>".

		this.commandToggle('formatBlock', 'blockquote');
		// this.commandToggle('insertHTML', '<br />');
	},

	link_insert: function () {
		var link = prompt("Enter the URL to create a link to:");
		if (!link) return;

		this.commandToggle('CreateLink', link);
	},

});
