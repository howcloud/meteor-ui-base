/**  */

/**

RichTextarea_Render
Renders content which was made in a RichTextarea
Takes care of block quotes, styling, late maths etc

**/

RichTextarea_Render = React.createClass({

	/** Component Properties **/

	propTypes: {
		value: React.PropTypes.string
	},
	getDefaultProps: function () {
		return {
			value: ''
		}
	},

	/** Mixins **/

	mixins: [React.addons.PureRenderMixin],

	/** State **/

	getInitialState: function () {
		return {
			value: this.processValue(this.props.value)
		}
	},

	/** Process Value State **/

	refreshValue: function (_value) {
		
		/** Set new value state **/

		this.setState({
			value: this.processValue(_value)
		});

	},

	processValue: function (_value) {
		if (!_value) _value = '';

		/** Process Math **/

		_value = processLatex(_value);
		_value = _value.replace("<a", "<a target=\"_blank\"");

		/** Return **/

		return _value;
	},

	/** React **/

	// componentWillMount: function () {
	// 	this.refreshValue(this.props.value); // anything which setStates in componentWillMount should not be done on server due to bug in React 0.13
	// },
	componentWillReceiveNewProps: function (nextProps) {
		this.refreshValue(nextProps.value);
	},

	// When we receive a new .value we should transform it and set the state as .value

	/** Render **/

	render: function () {
		var className = (this.props.className ? this.props.className + ' ' : '') + 'rtextarea-render';
		return (<div className={className} dangerouslySetInnerHTML={{__html: this.state.value}} />);
	},

});
