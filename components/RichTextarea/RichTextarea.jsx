/**  */

/**

RichTextarea
Heavily based on/inspired by https://github.com/mindmup/bootstrap-wysiwyg/blob/master/bootstrap-wysiwyg.js
See https://developer.mozilla.org/en-US/docs/Web/API/document.execCommand for main command reference

**/

/** ** ** ** **/

// {command: keyCode} // keyCode is a ctrl/meta+code that triggers this command state

var Commands = {
	'bold': 66, // b = 66
	'underline': 85, // u = 85
	'italic': 73, // i = 73
	'strikeThrough': 83, // s = 83
	'insertOrderedList': null, // ol (numbered)
	'insertUnorderedList': null, // ul (bullets)
	'formatBlock': null, // quote
}

/** ** ** ** **/

RichTextarea = React.createClass({

	/** Component Properties **/

	propTypes: {
		width: React.PropTypes.number,
		defaultValue: React.PropTypes.string,
		disabled: React.PropTypes.bool,
	},
	getDefaultProps: function () {
		return {
			defaultValue: '',
		}
	},

	/** State **/

	getInitialState: function () {
		return {
			commandState: {}, // command key to true or false, ie whether the command is currently toggled
			showPlaceholder: (this.props.placeholder ? (this.props.defaultValue ? false : true) : false),
		};
	},

	/** Focus **/

	isFocus: function () {
		return (document.activeElement === ReactDOM.findDOMNode(this.refs.textarea));
	},
	focus: function () {
		ReactDOM.findDOMNode(this.refs.textarea).focus();
	},
	blur: function () {
		ReactDOM.findDOMNode(this.refs.textarea).blur();
	},

	/** Value **/

	value: function () {
		return ReactDOM.findDOMNode(this.refs.textarea).innerHTML;
	},
	setValue: function (_value) {
		ReactDOM.findDOMNode(this.refs.textarea).innerHTML = _value;
		this.refreshPlaceholder();
	},

	/** Placeholder **/

	refreshPlaceholder: function () {
		if (removeTags(this.value()).length) {
			if (this.state.showPlaceholder) {
				this.setState({
					showPlaceholder: false
				});
			}
		} else {
			if (!this.state.showPlaceholder) {
				this.setState({
					showPlaceholder: true
				});
			}
		}
	},

	/** Selection/Cursor **/

	selectedRange: null, // saved selected range
	getCurrentRange: function () {
		var sel = window.getSelection();
		if (sel.getRangeAt && sel.rangeCount) {
			return sel.getRangeAt(0);
		}
	},
	saveSelection: function () {
		this.selectedRange = this.getCurrentRange();
	},
	restoreSelection: function () {
		var selection = window.getSelection();

		if (this.selectedRange) {
			try {
				selection.removeAllRanges();
			} catch (ex) {
				document.body.createTextRange().select();
				document.selection.empty();
			}

			selection.addRange(this.selectedRange);
		}
	},

	/*

	This doesn't work... TODO!

	moveCursor: function (spaces) {
		var node = this.refs.textarea.getDOMNode();

		this.focus();
		this.restoreSelection();
		var currPos = node.selectionStart;

		console.log(node);

		node.setSelectionRange(currPos+spaces, currPos+spaces); // assumes we are focused/currently selecting somewhere in the textarea
	},

	*/

	/** Commands **/

	execCommand: function (command, value) {
		this.restoreSelection();
		this.focus();

		if (command == 'equation') { // pseudo command to insert an equation holder
			document.execCommand('insertText', 0, '$$ $$');
			// this.moveCursor(6); // ie move forward by [math]
		} else {
			document.execCommand(command, 0, value); // , args [ie for inserting pics, a url]
		}

		this.refreshToolbar();
	},

	/** Toolbar **/

	refreshToolbar: function () {
		if (!this.isFocus()) return false;

		var commands = _.keys(Commands);
		var _commandState = {};
		
		_.each(commands, function (command) {
			_commandState[command] = document.queryCommandState(command);
		});

		this.setState({commandState: _commandState});
	},

	/** React **/

	shouldComponentUpdate: function (nextProps, nextState) {
		return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
	},
	componentWillUnmount: function () {
		this.blur();
	},

	/** Render **/

	render: function () {
		var inputClasses = classNames({
			'rtextarea-input': true,
			'input-error': this.props.error,
			'rtextarea-input-disabled': (this.props.disabled),
		});

		var classes = (this.props.className ? this.props.className + ' ' : '') + "rtextarea";

		return (
			<div className={classes}>
				<RichTextarea_Toolbar commandState={this.state.commandState} onCommandToggle={this.toolbar_toggleCommand} disabled={this.props.disabled} />

				<div className="rtextarea-input-contain">
					<div ref="textarea" 
						 className={inputClasses} 
						 contentEditable={this.props.disabled ? "false" : "true"} 
						 dangerouslySetInnerHTML={{__html: this.props.defaultValue}}

						 onMouseUp={this.textarea_mouseEvent} 
						 onMouseOut={this.textarea_mouseEvent} 
						 onKeyDown={this.textarea_keydown}
						 onKeyUp={this.textarea_keyup}

						 onPaste={this.onPaste} />

					{this.state.showPlaceholder && this.props.placeholder ? <div className="rtextarea-placeholder">{this.props.placeholder}</div> : null}
				</div>
			</div>
		);
	},

	/** UI Events **/

	textarea_keydown: function (sEvent) {
		setTimeout(this.refreshPlaceholder, 0);

		this.saveSelection();
		if (!sEvent.metaKey && !sEvent.ctrlKey) return true;

		var acted = false;
		var execCommand = this.execCommand;

		_.each(Commands, function (key, command) {
			if (!key) return;
			if (sEvent.keyCode == key) {
				execCommand(command);
				acted = true;
			}
		});
		
		if (acted) {
			sEvent.preventDefault();
			sEvent.stopPropagation();
		}

		this.refreshToolbar();
	},
	textarea_keyup: function (sEvent) {
		if (!this.props.placeholder) return;
		//this.refreshPlaceholder();
	},
	textarea_mouseEvent: function () {
		this.saveSelection();
		this.refreshToolbar();
	},

	toolbar_toggleCommand: function (command, value) {
		this.execCommand(command, value);
	},

	onPaste: function (sEvent) {
		sEvent.preventDefault();
		var text = sEvent.clipboardData ? sEvent.clipboardData.getData('text/plain') : prompt('What do you want to paste?');
		this.execCommand('insertText', text);
	},

});
