/**
 * 
 */

/**

Tag

**/

Tag = React.createClass({

	/** Component Properties **/

	propTypes: {
		size: React.PropTypes.oneOf(['regular', 'small']),
		href: React.PropTypes.string,

		allowDelete: React.PropTypes.bool,
		deleteCallback: React.PropTypes.func,
	},
	getDefaultProps: function () {
		return {
			size: 'regular'
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
	],

	/** Initial State **/

	// getInitialState: function () {
	// 	return {
			
	// 	}
	// },

	/** Render **/
	
	render: function () {
		var classes = {
			'tag': true,
			'tag-small': this.props.size === 'small' ? true : false
		}

		if (this.props.className) classes[this.props.className] = true;
		classes = classNames(classes);

		var deleteButton = this.props.allowDelete ? (<ButtonShell onClick={this.props.deleteCallback} size="small" color="red">
			<span style={{paddingTop: 0, paddingBottom: 0}}>
				<FontAwesome icon="times" style={{marginTop: -4}} />
			</span>
		</ButtonShell>) : null;

		var style = this.props.style || {}
		style = _.extend({
			paddingRight: this.props.allowDelete ? 1 : null,
		}, style);

		if (this.props.href && !deleteButton) {
			return (
				<a href={this.props.href} className={classes} style={style}>
					{this.props.children}
				</a>
			);
		} else {
			// we force this layout if we have a delete button so that we don't click through to the topic by accident when we are deleting/in edit mode

			return (
				<span className={classes} style={style}>
					{this.props.children} {deleteButton}
				</span>
			);
		}
	},

});
