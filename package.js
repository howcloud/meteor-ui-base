Package.describe({
	name: "howcloud:ui-base",
	version: "0.1.0",
	summary: "Exports React UI components and CSS which are common to HowCloud projects across apps"
});

Package.on_use(function (api) {
	
	/* Package Dependencies */

	api.use('meteor');
	api.use('underscore');
	api.use('less');
	
	api.use('jquery');
	api.use('mizzao:jquery-ui');
	
	api.use('mquandalle:bower'); // needed to load in dependencies for some of the libraries in here - todo: bundle these deps up within this package directly...?
	api.use('maxharris9:object-assign');

	api.use('howcloud:react'); // so we can do the actual rendering, although really any sort of react would do - doesn't have to be our HC version
	api.use('howcloud:react-deps');
	api.use('howcloud:react-build');
	api.use('howcloud:router'); // so that SiteController is defined to use for render delegation etc

	/** Add files **/

	var libPath = 'lib/';
	var mixinPath = libPath + '/mixins/';
	var utilPath = libPath + '/utils/';
	var componentPath = 'components/';
	var cssPath = 'css/';

	var assetFiles_client = [

		// TODO: Move social stuff to seperate package ???

		utilPath + 'Facebook.js', // NOTE: Assumes existence of a HCConfig.facebookAppId variable
		utilPath + 'Twitter.js',
		utilPath + 'LinkedIn.js',

		utilPath + 'launchModal.jsx',
		utilPath + 'promptShare.jsx',

		/************************************************************************/

		cssPath + 'base.css',
		cssPath + 'animation.css',
		cssPath + 'containers.css',
		cssPath + 'global.css',
		cssPath + 'font-awesome.min.css',
		cssPath + 'splash.css',
		cssPath + 'scrollSpy.css',
		cssPath + 'header-banner.css',
		cssPath + 'overlays.css',

		cssPath + 'inputs.less',
		
		cssPath + 'react-widgets/bootstrap-theme.import.less',
		cssPath + 'react-widgets/core.import.less',
		cssPath + 'react-widgets/datepicker.import.less',
		cssPath + 'react-widgets/icons.import.less',
		cssPath + 'react-widgets/mixins.import.less',
		cssPath + 'react-widgets/multiselect.import.less',
		cssPath + 'react-widgets/normalize.import.less',
		cssPath + 'react-widgets/popup.import.less',
		cssPath + 'react-widgets/selectlist.import.less',
		cssPath + 'react-widgets/variables.import.less',
		cssPath + 'react-widgets/react-widgets.less', // assumes we are using react widgets in the app (imported via bower on the app level [not currently doable at this package level, which would be neater])

	];

	var assetFiles_shared = [

		mixinPath + 'DimensionsChanged_mixin.js',
		mixinPath + 'FrameChanged_mixin.js',
		mixinPath + 'Position_bySelector.js',
		mixinPath + 'ScrollParent_onScrollBottom.js',
		mixinPath + 'ScrollSpy_Mixin.js',
		mixinPath + 'WindowPositionCentre_mixin.js',

		/************************************************************************/

		libPath + 'nuka-carousel/nuka-carousel.jsx',
		libPath + 'react-components/TimeoutTransitionGroup.jsx',

		/************************************************************************/

		componentPath + 'Accordian.jsx',
		componentPath + 'Bullets.jsx',
		componentPath + 'Button.jsx',
		componentPath + 'EqualBlocks.jsx',
		componentPath + 'FontAwesome.jsx',
		componentPath + 'OverlayContainer.jsx',
		componentPath + 'Paragraphs.jsx',
		componentPath + 'Popover.jsx',
		componentPath + 'SizeAwareDiv.jsx',
		componentPath + 'Slider.jsx',
		componentPath + 'Tag.jsx',
		componentPath + 'HeightAlign.jsx',
		componentPath + 'PositionInMiddle.jsx',
		componentPath + 'CheckCircle.jsx',

		componentPath + 'ReactWidgets.js', // does not in and of itself assume ReactWidgets is loaded in (but we do load in the CSS everytime)
										   // globalises the components if it is, though

		componentPath + 'Inputs/SuggestInput.jsx',
		componentPath + 'Inputs/TextInput.jsx',
		componentPath + 'Inputs/TextSelect.jsx',

		componentPath + 'Header_Banner/Header_Banner.jsx',
		componentPath + 'Header_Banner/Header_Banner_Tagline.jsx',

		componentPath + 'Modal/Modal.jsx',
		componentPath + 'Modal/ModalContentBox.jsx',

		componentPath + 'RichTextarea/RichTextarea.jsx',
		componentPath + 'RichTextarea/RichTextarea_Render.jsx',
		componentPath + 'RichTextarea/RichTextarea_Toolbar.jsx',

		componentPath + 'Social/Facebook_Like.jsx',
		componentPath + 'Social/Facebook_Send.jsx',
		componentPath + 'Social/Facebook_Share.jsx',
		componentPath + 'Social/LinkedIn_Share.jsx',
		componentPath + 'Social/Social_Boxes.jsx',
		componentPath + 'Social/Twitter_Tweet.jsx',

		componentPath + 'Splash/Splash.jsx',
		componentPath + 'Splash/Splash_Headered.jsx',
		componentPath + 'Splash/Splash_Nav.jsx',
		componentPath + 'Splash/SplashTile.jsx',
		componentPath + 'Splash/ContentSplash.jsx',

		componentPath + 'Toolbar/Toolbar.jsx',
		componentPath + 'Toolbar/Toolbar_Button.jsx',
		componentPath + 'Toolbar/Toolbar_Elem.jsx',
		componentPath + 'Toolbar/Toolbar_ElemContainer.jsx',
		componentPath + 'Toolbar/Toolbar_Text.jsx',

		componentPath + 'Video/Video_Player.jsx',
		componentPath + 'Video/Video_Thumb.jsx',
		componentPath + 'Video/Video_ThumbLaunchModal.jsx',
		componentPath + 'Video/Video_ThumbPlayInplace.jsx',

	];

	api.addFiles(assetFiles_client, ['client']);
	api.addFiles(assetFiles_shared, ['server', 'client']);

	/** Export **/

	var exports_client = [
		'promptShare',
		'launchModal',

		// TODO: Move these to a separate package

		'initFacebookTagApi',
		'deferUntilFBInit',
		'deferUntilLinkedInInit'
	];

	var exports_shared = [

		'DimensionsChanged_mixin',
		'SizeChanged_mixin', // new name for DimensionsChanged_mixin
		'FrameChanged_mixin',
		'Frame', // defined in FrameChanged_mixin.js
		'Position_bySelector_mixin',
		'ScrollParent_onScrollBottom_Mixin',
		'ScrollSpy_Mixin',
		'WindowPositionCentre_mixin',

		'jQueryElemIsFixed', // defined in Position_bySelector.js and we use it in guides (for example) so lets export it for now (todo: move into a more general lib etc)

		/************************************************************************/

		// nuka-carousel
		'Carousel',
		'Carousel_defaultDecorators_noDots',
		'Carousel_defaultDecorators',
		'Carousel_minimalDecorators',

		// khan academey react-components
		'TimeoutTransitionGroup',

		/************************************************************************/

		'Header_Banner',
		'Header_Banner_Tagline',

		'Modal',
		'ModalContentBox',

		'Toolbar',
		'Toolbar_Button',
		'Toolbar_Elem',
		'Toolbar_ElemContainer',
		'Toolbar_Text',

		'Video_Player',
		'Video_Thumb',
		'Video_ThumbLaunchModal',
		'Video_ThumbPlayInplace',

		'RichTextarea',
		'RichTextarea_Render',

		'Facebook_Like',
		'Facebook_Send',
		'Facebook_Share',
		'LinkedIn_Share',
		'Social_Boxes',
		'Twitter_Tweet',

		'Splash',
		'Splash_Headered',
		'Splash_Nav',
		'SplashTile',
		'ContentSplash',

		'Accordian',
		'Bullets',
		'Button',
		'ButtonShell',
		'EqualBlocks',
		'FontAwesome',
		'OverlayContainer',
		'Paragraphs',
		'Popover',
		'SizeAwareDiv',
		'Slider',
		'Tag',
		'HeightAlign',
		'PositionInMiddle',
		'CheckCircle',

		'TextInput',
		'SuggestInput',
		'TextSelect',

		// React Widgets
		'DropdownList',
		'Combobox',
		'NumberPicker',
		'Multiselect',
		'SelectList',
		'Calendar',
		'DateTimePicker'

	];

	api.export(exports_client, ['client']);
	api.export(exports_shared, ['server', 'client']);

});