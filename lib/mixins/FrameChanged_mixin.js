/**

FrameChanged_mixin
Returns a frame dimension from getFrame() which defines some space in the component for which we want to provide callbacks on position relative to THIS COMPONENTS coordinate space
See definition of frame vs bounds like on iOS: http://stackoverflow.com/questions/5361369/uiview-frame-bounds-and-center

frame = {
	top,
	left,

	width,
	height
}

**/

FrameChanged_mixin = {

	/** Dimensions Changed Callback **/

	_frames: null,

	_refreshFrames: function (_width, _height) {
		this._frames = this._frames || {}

		_.each(this.constructor.frames, function (_callbackName, _frameName) {
			if (!this.props[_callbackName]) return;

			var frame = this[_frameName]();
			if (!_.isEqual(frame, this._frames[_frameName])) {
				this.props[_callbackName](frame);
				this._frames[_frameName] = frame;
			}
		}.bind(this));
	},

	/** React **/

	componentDidMount: function () {
		this._refreshFrames();
	},
	componentDidUpdate: function (prevProps, prevState) {
		this._refreshFrames();
	},

}

/** Empty Frame **/

Frame = function (_width, _height, _top, _left) {
	_width = _width || 0;
	_height = _height || 0;
	_top = _top || 0;
	_left = _left || 0;

	this.width = _width;
	this.height = _height;
	this.top = _top;
	this.left = _left;
}
