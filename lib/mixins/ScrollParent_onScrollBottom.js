/**

ScrollParent_onScrollBottom_Mixin

Method to react to a component's scroll parent reaching a position such that the bottom of this component is in view
Good for implementing infinite scroll interfaces

Assumes the component which mixes this in implements two functions: 
	shouldListenForScrollBottom: returns true or false depending on if we care about scrolling to the bottom of the component at the moment
	onScrollBottom: the function to react to the fact we have now scrolled to the bottom of a component

**/

ScrollParent_onScrollBottom_Mixin = {

	/** React **/

	componentDidMount: function () {
		var node = this;
		if (this.refs && this.refs.listenScrollBottom) node = this.refs.listenScrollBottom;

		var $node = jQuery(ReactDOM.findDOMNode(node));
		this.$scrollParent = $node.scrollParent();
		this.$scrollParent.on("scroll", this.onScroll);
	},
	componentWillUnmount: function () {
		if (this.$scrollParent) this.$scrollParent.unbind("scroll", this.onScroll);
	},

	/** JQuery Node Cache **/

	// [TODO: Move to a separate mixin?]

	$node: null,
	$scrollParent: null,
	
	getJQueryNode: function () {
		if (!this.$node) this.$node = jQuery(ReactDOM.findDOMNode(this));
		return this.$node;
	},

	/** Scroll Listener **/

	onScroll: function () {
		if (!this.$scrollParent) return; // shouldn't ever happen really
		if (!this.onScrollBottom) return; // could just assume this is defined
		if (this.shouldListenForScrollBottom && !this.shouldListenForScrollBottom()) return; // no need to consider loading more

		var $node = this.getJQueryNode();
		var $scrollParent = this.$scrollParent;
		
		// window.$node = $node;
		// window.$scrollParent = $scrollParent;

		var scrollParentIsDocument = $scrollParent.is(jQuery(document));

		var scrollParentOffset = $scrollParent.offset() ? Math.abs($scrollParent.offset().top) : 0;
		var visibleHeight = scrollParentIsDocument ? jQuery(window).height() : $scrollParent.outerHeight();
		var scrollBottom = visibleHeight + scrollParentOffset; // $scrollParent.scrollTop() +

		var nodeOffset = $node.offset().top;
		if (scrollParentIsDocument) nodeOffset -= $scrollParent.scrollTop();

		var nodeBottom = nodeOffset + $node.outerHeight() - 30;

		if (scrollBottom > nodeBottom) this.onScrollBottom();

		// OLD

		// var $node = this.getJQueryNode();
		// var thisHeight = $node.outerHeight();
		
		// // relative position is the position of the our container within its scroll parent
		// var scrollParentOffset = this.$scrollParent.offset() ? this.$scrollParent.offset().top : 0; // might be undefined if the scrollParent is the document 
		// var thisBottom = $node.offset().top + thisHeight;
	
		// var scrollParentVisibleHeight;
		// try { // jQuery errors out if the scrollParent is the document and we try to get its position
		// 	  // TODO: Just cache a flag if the scrollParent is the document as it simplifies a lot of this onScroll function

		// 	scrollParentVisibleHeight = this.$scrollParent.height() - this.$scrollParent.position().top;
		// } catch (err) {
		// 	scrollParentVisibleHeight = jQuery(window).height();
		// }
		 
		// var scrollTop = this.$scrollParent.scrollTop();
		// if (thisBottom > scrollTop && thisBottom < scrollTop + scrollParentVisibleHeight + scrollParentOffset) this.onScrollBottom();
	},

}
