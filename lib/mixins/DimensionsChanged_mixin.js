/**

DimensionsChanged_mixin
Deals with common 

Expects: getDimensions to accept state and props and return an object width width and height


**/

DimensionsChanged_mixin = SizeChanged_mixin = {

	/** Dimensions Changed Callback **/

	// our internal record of what width and height we have declared this sidebar element has:

	_width: 0,
	_height: 0,

	_dimensionsChanged: function (_width, _height) {
		if (!this.props.onDimensionsChange && !this.props.onSizeChange) return;

		this._width = _width;
		this._height = _height;

		if (this.props.onDimensionsChanged) this.props.onDimensionsChange(_width, _height);
		if (this.props.onSizeChange) this.props.onSizeChange(_width, _height);
	},

	/** React **/

	componentDidMount: function () {
		var dimensions = this.getDimensions(this.props, this.state);
		this._dimensionsChanged(dimensions.width, dimensions.height);
	},
	componentDidUpdate: function (prevProps, prevState) {
		var dimensions = this.getDimensions();

		if (dimensions.width !== this._width || dimensions.height !== this._height) this._dimensionsChanged(dimensions.width, dimensions.height);
	},

}
