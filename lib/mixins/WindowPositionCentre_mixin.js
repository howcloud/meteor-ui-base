/**

Works out the top left positioning for an element and sets a top left location
Element/node is set by the ref 'positionCentre' or if not it's the root mounted node for the component

**/

WindowPositionCentre_mixin = {

	/** Props/State **/

	getInitialState: function () {
		return {
			top: 0,
			left: 0,
		}
	},

	/** Refresh Position **/

	_positionNode: function () {
		return this.refs && this.refs._positionCentre ? this.refs._positionCentre : this;
	},

	refreshPosition: function () {
		if (!this.isMounted()) return null;

		var node = this._positionNode();
		var $node = jQuery(ReactDOM.findDOMNode(node));
		var childWidth = $node.outerWidth();
		var childHeight = $node.outerHeight();

		var $window = jQuery(window);
		var windowWidth = $window.width();
		var windowHeight = $window.height();

		// console.log("child "+childWidth+"x"+childHeight+", window "+windowWidth+"x"+windowHeight);

		this.setState({
			top: ((windowHeight-childHeight)/2.0),
			left: ((windowWidth-childWidth)/2.0),

			// Extra details set as they can be useful
			windowWidth: windowWidth,
			windowHeight: windowHeight,
			childWidth: childWidth,
			childHeight: childHeight
		});
	},

	/** React **/

	/*componentDidUpdate: function (prevProps, prevState) {
		// this.refreshPosition(); // issue of this recursively continuously calling
	},*/
	componentDidMount: function () {
		this.refreshPosition();
		window.addEventListener('resize', this.refreshPosition);
	},
	componentWillUnmount: function () {
		window.removeEventListener('resize', this.refreshPosition);
	},

}
