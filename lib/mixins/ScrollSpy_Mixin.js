/**

ScrollSpy_Mixin

Method to track a number of refs in the component as the user scrolls the component
We define scrolling the component as scrolling the scrollParent for the element with ref scrollSpyContainer (or if that doesn't exist it's just the mounted node)
The refs we listen for their scroll focus are defined as an array at the component level, this.scrollSpyOn
When a certain ref is pulled into focus we set this.state.scrollSpyFocus = that ref name

Assumes the component which mixes this in implements two functions: 
	scrollSpyOn: array of strings of refs for elements we want to spy on
				 we assume these are in order of where they are on the page (ie element 0 is the first, 1 is the second etc, doesn't matter if undefined as a ref sometimes as we skip those)
				 OR an object of refs to the amount they offset the scrollSpyOffset by in virtue of us triggering their focus (ie because they are now affixed) 
	scrollSpyOffset: (optional) the amount by which to offset the top of the scrollSpyContainer DOM node

**/

// TODO: Refactor some of this (especially onScroll and scrollToSection)

ScrollSpy_Mixin = {

	/** React **/

	componentDidMount: function () {
		var node = this;
		if (this.refs && this.refs.scrollSpyContainer) node = this.refs.scrollSpyContainer;

		this.scrollSpy_$node = jQuery(ReactDOM.findDOMNode(node));

		this.scrollSpy_$scrollParent = this.scrollSpy_$node.scrollParent();
		// console.log(this.scrollSpy_$scrollParent);
		this.scrollSpy_$scrollParent.on("scroll", this.scrollSpy_onScroll);

		this.scrollSpy_onScroll(); // call it straight away in case some components need it
	},
	componentWillMount: function () {
		if (!this.scrollSpyOn) return;

		var focusRef = this.defaultRef();
		if (Meteor.isClient) this.setState({scrollSpyFocus: focusRef}); // assume that we are at the top of the page by default, ie no scroll on load
																		// ONLY DOING SET STATE ON CLIENT DUE TO BUG IN REACT 0.13 - should be okay to do on server too in 0.14
	},
	componentWillUnmount: function () {
		if (this.scrollSpy_$scrollParent) this.scrollSpy_$scrollParent.unbind("scroll", this.scrollSpy_onScroll);
	},
	// componentDidUpdate: function(prevProps, prevState) {
	// 	this.scrollSpy_onScroll();
	// },

	/** Utils **/

	defaultRef: function () {
		if (this.scrollSpyDefaultFocus) {
			return this.scrollSpyDefaultFocus;
		} else if (this.scrollSpyOn instanceof Array) {
			return this.scrollSpyOn[0];
		}
		
		return _.keys(this.scrollSypOn)[0];
	},

	/** JQuery Node Cache **/

	scrollSpy_$scrollParent: null,

	/** Scroll Listener **/

	scrollSpy_onScroll: function () {
		if (!this.scrollSpy_$scrollParent) return; // shouldn't ever happen really
		if (!this.scrollSpyOn) return; // could just assume this is defined
		if (!this.refs) return;

		var scrollSpyOffset = this.scrollSpyOffset || 0; // this is the amount by which we can offset the scrollSpy container's 'top' by
														 // can also be set on a per element basic by using an object vs array for scrollSpyOn

		var scrollParentOffset = this.scrollSpy_$scrollParent.offset() ? this.scrollSpy_$scrollParent.offset().top : 0; // might be undefined if the scrollParent is the document 
		var scrollParentScroll = this.scrollSpy_$scrollParent.scrollTop();

		// console.log("scrollSpy_$node.offset.top = " + this.scrollSpy_$node.offset().top  + " scrollParentOffset = " + scrollParentOffset);

		var scrollSpyOnOffset = this.scrollSpy_$scrollParent.offset() ? this.scrollSpy_$node.offset().top - scrollParentOffset : 0; // very important when not using a scroll parent which is itself the body - in these cases we need to know where our element has been scrolled relevant to the body
																																	// we adjust this - scrollParentOffset so that we don't double count scrollParentOffset later on in our iteraction
																																	// NOTE: we now only do this if the scroll parent NOT the body (ie we check that an offset is defined on the body first)

		// console.log("scroll parent scroll = "+scrollParentScroll);
		// console.log("scroll parent offset = "+scrollParentOffset);

		var isArray = this.scrollSpyOn instanceof Array ? true : false;

		var focusedRef = null;
		var previousRef = this.defaultRef();

		_.every(this.scrollSpyOn, function (value, key) {
			var ref = isArray ? value : key;

			var node = this.refs[ref];
			if (!node) return true; // continue 

			var $node = jQuery(ReactDOM.findDOMNode(node));
			if (!$node.is(':visible')) return true; // continue
			
			// nodeOffset is our offset relative to the scrollParent
			var nodeOffset = $node.offset().top - scrollParentOffset - scrollSpyOnOffset;

			// relativeOffset is the position of the our container within its scroll parent
			var relativeOffset = nodeOffset - scrollSpyOffset;
			
			// console.log({
			// 	nodeOffsetTop: $node.offset().top,
			// 	scrollParentOffset: scrollParentOffset,
			// 	scrollSpyOnOffset: scrollSpyOnOffset,
			// 	scrollSpyOffset: scrollSpyOffset,
			// 	scrollParentScroll: scrollParentScroll,
			// 	relativeOffset: relativeOffset
			// });
			// console.log("looking at "+ref+" relative offset = "+relativeOffset);

			var elemOffset = isArray ? key : value;
			elemOffset = isArray ? 0 : elemOffset;
			scrollSpyOffset += elemOffset;

			if (!relativeOffset === scrollParentScroll) {
				focusedRef = ref;

				return false; // break
			} else if (scrollParentScroll < relativeOffset) {
				focusedRef = previousRef ? previousRef : ref;

				return false; // break
			} else {
				previousRef = ref;

				return true; // continue [default action]
			}
		}.bind(this));

		if (!focusedRef) focusedRef = previousRef;
		// console.log("focused on " + focusedRef);
		if (this.state.scrollSpyFocus !== focusedRef) this.setState({scrollSpyFocus: focusedRef});
	},

	/** Scroll to Function **/

	scrollToSection: function (ref) {
		if (!this.refs) return;

		var node = this.refs[ref];
		if (!node) return;

		// Find the scrollSpyOffset for this ref (we need to total up the offsets produced by scrolling through each of our spy points up to here)

		var scrollSpyOffset = this.scrollSpyOffset || 0;

		var isArray = this.scrollSpyOn instanceof Array ? true : false;
		_.every(this.scrollSpyOn, function (value, key) {
			var _ref = isArray ? value : key;

			var node = this.refs[_ref];
			if (!node) return true; // continue

			var $node = jQuery(ReactDOM.findDOMNode(node));
			if (!$node.is(':visible')) return true; // continue

			var elemOffset = isArray ? key : value;
			elemOffset = isArray ? 0 : elemOffset;
			scrollSpyOffset += elemOffset;

			if (_ref === ref) return false; // break

			return true;
		}.bind(this));

		// Scroll!		

		var $node = jQuery(ReactDOM.findDOMNode(node));
		var scrollSpyOnOffset = this.scrollSpy_$node.offset().top; // see importance explanation in onScroll function

		var _scrollTop = $node.offset().top - scrollSpyOffset - scrollSpyOnOffset;
		if (_scrollTop < 0) _scrollTop = 0;
		
		// console.log("node.offset.top = " + $node.offset().top);
		// console.log("scroll spy offset = " + scrollSpyOffset);
		// console.log("scrollTop = " + _scrollTop);

		// if body - we scroll html, body; else we just scroll the parent
		// we use a hack that the body does not have an offset defined to work out if the parent is the body
		var $scroll = this.scrollSpy_$scrollParent.offset() ? this.scrollSpy_$scrollParent : jQuery('html, body');
		
		$scroll.animate({
			scrollTop: _scrollTop
		}, 750);
	},

}
