/**

Position_bySelector
Allows us to position a node by a selector given a clock position

Assumes the component mixing in defines:
	_positionSelector: the selector to position next to [available at componentWillMount]
	_positionSelectorAnchor: clock position to position by [available at componentWillMount]

Optional:
	_positionOffset = {} // object {top:, left:}

**/

_anchorNameMapping = {
	"topLeft": 11,
	"top": 12,
	"topRight": 1,
	"rightTop": 2,
	"right": 3,
	"rightBottom": 4,
	"bottomRight": 5,
	"bottom": 6,
	"bottomLeft": 7,
	"leftBottom": 8,
	"left": 9,
	"leftTop": 10
};

// Returns in form [top, left]
var _getOffset = function (anchor, positionerWidth, positionerHeight, bufferOffset, myWidth, myHeight) {
	if (anchor === 1) return [-bufferOffset - myHeight, positionerWidth - myWidth];
	if (anchor === 2) return [0, bufferOffset + positionerWidth];
	if (anchor === 3) return [positionerHeight/2 - myHeight/2, bufferOffset + positionerWidth];
	if (anchor === 4) return [positionerHeight - myHeight, bufferOffset + positionerWidth];
	if (anchor === 5) return [bufferOffset + positionerHeight, positionerWidth - myWidth];
	if (anchor === 6) return [bufferOffset + positionerHeight, positionerWidth/2 - myWidth/2];
	if (anchor === 7) return [bufferOffset + positionerHeight, 0];
	if (anchor === 8) return [positionerHeight - myHeight, -myWidth - bufferOffset];
	if (anchor === 9) return [positionerHeight/2 - myHeight/2, -myWidth - bufferOffset];
	if (anchor === 10) return [0, -myWidth - bufferOffset];
	if (anchor === 11) return [-bufferOffset - myHeight, 0];
	if (anchor === 12) return [-bufferOffset - myHeight, positionerWidth/2 - myWidth/2];
}

// Checks if the element or any of its parents are fixed position => should show positioned element in a fixed position
jQueryElemIsFixed = function ($element) { // $element is a jQuery element
	if ($element.css("position") === "fixed") return true;

	var $parent = $element.offsetParent();
	if ($parent && !$parent.is($element)) return jQueryElemIsFixed($parent);

	return false;
}

Position_bySelector_mixin = {

	/** Refresh Position **/

	setPositionSelector: function (selector, anchor, doNotRefresh) {
		// it is at this point that if this._positionSelector !== selector then we should deal with unbinding from any events we listen on with that element (ie scrolling of its scroll parent)

		this._positionSelector = selector;
		this._positionSelectorAnchor = anchor;

		if (!doNotRefresh) this.refreshPosition();
	},

	refreshPosition: function () {
		// Get window

		var $window = jQuery(window);

		// Get this node

		var node = this.getPositionedNode ? this.getPositionedNode() : this;
		if (!node) return;

		var $node = jQuery(ReactDOM.findDOMNode(node));
		var thisWidth = $node.innerWidth();
		var thisHeight = $node.innerHeight();

		// Get base data

		var data = {
			positionTop: 0,
			positionLeft: 0,
			positionType: 0,

			selectorTop: 0,
			selectorLeft: 0,

			thisWidth: thisWidth,
			thisHeight: thisHeight,

			// might as well - we use it in Guide_Popover
			windowWidth: $window.width(),
			windowHeight: $window.height(),
		}

		if (!this._positionSelector) {
			this.setState(data);
			return;
		}

		// Get jQuery node to position relative to

		var $positioner = jQuery(this._positionSelector);
		if (!$positioner.length) return;

		// Get initial position

		var positionerOffset = $positioner.offset();

		// save these to set as part of state for focusing etc
		var selectorTop = positionerOffset.top; 
		var selectorLeft = positionerOffset.left;

		// these ones we mutate given our anchor etc
		var top = selectorTop;
		var left = selectorLeft;

		// Get position relative to anchor

		var anchor = this._positionSelectorAnchor;
		if (_anchorNameMapping[anchor]) anchor = _anchorNameMapping[anchor];
		if (!anchor) anchor = 6;

		var positionerWidth = $positioner.innerWidth();
		var positionerHeight = $positioner.innerHeight();
		var bufferOffset = 13; // can be used to buffer this from the positioned node, leftover from adapting guiders code

		var offset = _getOffset(anchor, positionerWidth, positionerHeight, bufferOffset, thisWidth, thisHeight);
		top += offset[0];
		left += offset[1];

		// Get positioning type

		var positionType = 'absolute';
		if (jQueryElemIsFixed($positioner)) {
			positionType = 'fixed';

			top -= $window.scrollTop();
			left -= $window.scrollLeft();
		}

		// Additional positioning

		if (this._positionOffset) {
			top += this._positionOffset.x;
			left += this._positionOffset.y;
		}

		// Set state

		this.setState(_.extend(data, {
			positionTop: top,
			positionLeft: left,
			positionType: positionType,

			selectorTop: selectorTop,
			selectorLeft: selectorLeft
		}));
	},

	/** React **/

	/*componentWillMount: function () {
		//this.refreshPosition(); // don't want to do it here as we don't have this mounted yet to get width and height against	
	},*/
	componentDidMount: function () {
		this.refreshPosition();
		window.addEventListener('resize', this.refreshPosition);
	},
	componentWillUnmount: function () {
		window.removeEventListener('resize', this.refreshPosition);
	},
	// componentDidUpdate: function () {
	// 	this.refreshPosition(); // do we need to do this???
	// },
	componentDidUpdate: function (prevProps, prevState) {
		// when we change the children we change the width/height (is the assumption)

		if (!_.isEqual(this.props.children, prevProps.children)) this.refreshPosition();
	},

}
