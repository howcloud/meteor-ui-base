/**

Source adapted for Meteor/our setup from https://github.com/kenwheeler/nuka-carousel

**/

var assign = objectAssign;

// console.log(GLOBAL);
// We used to wrap in Meteor.startup because tweenState is not defined until startup
// But now as long as bower.json is located in /lib/ then our deps like tweenState will be loaded before this package
// ABOVE IS NOT TRUE / DOES NOT SEEM TO WORK
// So we resort to hack of wrapping in Meteor.startup and setting window. for the variables

var makeGlobal;
try {
  makeGlobal = window;
} catch (err) {
  makeGlobal = global;
}

Meteor.startup(function () {

  var addEvent = function(elem, type, eventHandle) {
    if (elem == null || typeof (elem) === 'undefined') {
      return;
    }
    if (elem.addEventListener) {
      elem.addEventListener(type, eventHandle, false);
    } else if (elem.attachEvent) {
      elem.attachEvent('on' + type, eventHandle);
    } else {
      elem['on'+type] = eventHandle;
    }
  };

  var removeEvent = function(elem, type, eventHandle) {
    if (elem == null || typeof (elem) === 'undefined') {
      return;
    }
    if (elem.removeEventListener) {
      elem.removeEventListener(type, eventHandle, false);
    } else if (elem.detachEvent) {
      elem.detachEvent('on' + type, eventHandle);
    } else {
      elem['on'+type] = null;
    }
  };

  /********************************************************************************************/
  /* Adapted slightly from default decorators that usually come with Nuka Carousel: */
     // We use a prop system to pass props into instances of components which are decorators 
     // We use fontawesome etc 

  var decorator_previousButton = {
    component: React.createClass({
      getDefaultProps: function () {
        return {
          background: 'rgba(0,0,0,0.4)',
          color: 'white',
        }
      },
      render: function () {
        return (
          <button
            style={this.getButtonStyles(this.props.currentSlide === 0)}
            onClick={this.props.previousSlide}><FontAwesome icon="chevron-left" /></button>
        )
      },
      getButtonStyles: function(disabled) {
        return {
          border: 0,
          background: this.props.background,
          color: this.props.color,
          padding: 10,
          outline: 0,
          opacity: disabled ? 0.3 : 1,
          cursor: 'pointer'
        }
      }
    }),

    position: 'CenterLeft'
  }

  var decorator_nextButton =  {
    component: React.createClass({
      getDefaultProps: function () {
        return {
          background: 'rgba(0,0,0,0.4)',
          color: 'white',
        }
      },
      render: function () {
        return (
          <button
            style={this.getButtonStyles(this.props.currentSlide + this.props.slidesToScroll >= this.props.slideCount)}
            onClick={this.props.nextSlide}><FontAwesome icon="chevron-right" /></button>
        )
      },
      getButtonStyles: function(disabled) {
        return {
          border: 0,
          background: this.props.background,
          color: this.props.color,
          padding: 10,
          outline: 0,
          opacity: disabled ? 0.3 : 1,
          cursor: 'pointer'
        }
      }
    }),

    position: 'CenterRight'
  }

  var decorator_absoluteDots = {
    component: React.createClass({
      getDefaultProps: function () {
        return {
          color: 'black'
        }
      },
      render: function () {
        var self = this;
        var indexes = _.range(self.props.slideCount);

        /** Styles **/

        var list_style = {
          position: 'relative',
          margin: 0,
          // top: -10,
          padding: 0
        }

        var item_style = {
          listStyleType: 'none',
          display: 'inline-block',
          verticalAlign: 'middle'
        }

        /** Render **/

        return (
          <ul style={list_style}>
            {_.map(indexes, function(index) {
                return (
                  <li style={item_style} key={index}>
                    <button
                      style={self.getButtonStyles(self.props.currentSlide === index)}
                      onClick={self.props.goToSlide.bind(null, index)}>
                      &bull;
                    </button>
                  </li>
                )
              })}
          </ul>
        )
      },
      getButtonStyles: function(active) {
        return {
          border: 0,
          background: 'transparent',
          color: this.props.color,
          cursor: 'pointer',
          padding: '0 10px',
          outline: 0,
          fontSize: active ? 27 : 24,
          lineHeight: active ? '27px' : '24px',
          opacity: active ? 1 : 0.5
        }
      }
    }),

    position: 'BottomCenter'
  }

  makeGlobal.Carousel_defaultDecorators = [
    decorator_previousButton,
    decorator_nextButton,
    decorator_absoluteDots
  ];

  makeGlobal.Carousel_defaultDecorators_noDots = [
    decorator_previousButton,
    decorator_nextButton
  ];

  makeGlobal.Carousel_decoratorsWithProps = function (_decorators, _props) {
    return _.map(_decorators, function (decorator) {
      return _.extend(_.clone(decorator), {
        props: _props
      });
    });
  }

  /***********/

  var decorator_minimal_previousButton = {
    component: React.createClass({
      getDefaultProps: function () {
        return {
          color: 'rgba(0,0,0,0.4)'
        }
      },
      render: function () {
        return (
          <button
            style={this.getButtonStyles(this.props.currentSlide === 0)}
            onClick={this.props.previousSlide}><FontAwesome icon="chevron-left" /></button>
        )
      },
      getButtonStyles: function(disabled) {
        return {
          border: 0,
          background: 'transparent',
          color: this.props.color,
          padding: 10,
          outline: 0,
          opacity: disabled ? 0.3 : 1,
          cursor: 'pointer',
          fontSize: 22
        }
      }
    }),

    position: 'CenterLeft'
  }

  var decorator_minimal_nextButton =  {
    component: React.createClass({
      getDefaultProps: function () {
        return {
          color: 'rgba(0,0,0,0.4)'
        }
      },
      render: function () {
        return (
          <button
            style={this.getButtonStyles(this.props.currentSlide + this.props.slidesToScroll >= this.props.slideCount)}
            onClick={this.props.nextSlide}><FontAwesome icon="chevron-right" /></button>
        )
      },
      getButtonStyles: function(disabled) {
        return {
          border: 0,
          background: 'transparent',
          color: this.props.color,
          padding: 10,
          outline: 0,
          opacity: disabled ? 0.3 : 1,
          cursor: 'pointer',
          fontSize: 22
        }
      }
    }),

    position: 'CenterRight'
  }  

  makeGlobal.Carousel_minimalDecorators = [
    decorator_minimal_previousButton,
    decorator_minimal_nextButton,
    decorator_absoluteDots
  ];

  /****************************************************************************************/

  makeGlobal.Carousel = React.createClass({
    displayName: 'Carousel',

    mixins: [makeGlobal.tweenState.Mixin],

    propTypes: {
      cellAlign: React.PropTypes.oneOf(['left', 'center', 'right']),
      cellSpacing: React.PropTypes.number,
      data: React.PropTypes.func,
      decorators: React.PropTypes.array,
      dragging: React.PropTypes.bool,
      easing: React.PropTypes.string,
      edgeEasing: React.PropTypes.string,
      framePadding: React.PropTypes.string,
      initialSlideHeight: React.PropTypes.number,
      initialSlideWidth: React.PropTypes.number,
      slidesToShow: React.PropTypes.number,
      slidesToScroll: React.PropTypes.number,
      slideWidth: React.PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.number
      ]),
      speed: React.PropTypes.number,
      vertical: React.PropTypes.bool,
      width: React.PropTypes.string
    },

    getDefaultProps: function () {
      return {
        cellAlign: 'left',
        cellSpacing: 0,
        data: function () {},
        decorators: Carousel_defaultDecorators,
        dragging: true,
        easing: 'easeInOutQuad', // was easeOutCirc
        edgeEasing: 'easeInOutBack', // wase easeOutElastic
        framePadding: '0px',
        slidesToShow: 1,
        slidesToScroll: 1,
        slideWidth: 1,
        speed: 500,
        vertical: false,
        width: '100%'
      }
    },

    getInitialState: function () {
      return {
        currentSlide: 0,
        dragging: false,
        frameWidth: 0,
        left: 0,
        top: 0,
        slideCount: 0,
        slideWidth: 0
      }
    },

    componentWillMount: function () {
      if (Meteor.isClient) this.setInitialDimensions();
    },

    componentDidMount: function () {
      this.setDimensions();
      this.bindEvents();
    },

    componentWillUnmount: function () {
      this.unbindEvents();
    },

    render: function () {
      var self = this;
      var children = this.formatChildren(this.props.children);
      return (
        <div className={['slider', this.props.className || ''].join(' ')} ref="slider" style={assign(this.getSliderStyles(), this.props.style || {})}>
          <div className="slider-frame"
            ref="frame"
            style={this.getFrameStyles()}
            {...this.getTouchEvents()}
            {...this.getMouseEvents()}
            onClick={this.handleClick}>
            <ul className="slider-list" ref="list" style={this.getListStyles()}>
              {children}
            </ul>
          </div>
          {this.props.decorators ?
            this.props.decorators.map(function(Decorator, index) {
              return (
                <div
                  style={assign(self.getDecoratorStyles(Decorator.position), Decorator.style || {})}
                  className={'slider-decorator-' + index}
                  key={index}>
                  {React.createElement(Decorator.component, _.extend({
                    currentSlide: self.state.currentSlide,
                    slideCount: self.state.slideCount,
                    slidesToShow: self.props.slidesToShow,
                    slidesToScroll: self.props.slidesToScroll,
                    nextSlide: self.nextSlide,
                    previousSlide: self.previousSlide,
                    goToSlide: self.goToSlide,
                  }, Decorator.props || {}))}
                </div>
              )
            })
          : null}
          <style type="text/css" dangerouslySetInnerHTML={{__html: self.getStyleTagStyles()}}/>
        </div>
      )
    },

    // Touch Events

    touchObject: {},

    getTouchEvents: function () {
      var self = this;

      return {
        onTouchStart: function(e) {
          self.touchObject = {
            startX: event.touches[0].pageX,
            startY: event.touches[0].pageY
          }
        },
        onTouchMove: function(e) {
          var direction = self.swipeDirection(
            self.touchObject.startX,
            e.touches[0].pageX,
            self.touchObject.startY,
            e.touches[0].pageY
          );

          if (direction !== 0) {
            e.preventDefault();
          }

          self.touchObject = {
            startX: self.touchObject.startX,
            startY: self.touchObject.startY,
            endX: e.touches[0].pageX,
            endY: e.touches[0].pageY,
            length: Math.round(Math.sqrt(Math.pow(e.touches[0].pageX - self.touchObject.startX, 2))),
            direction: direction
          }

          self.setState({
            left: self.props.vertical ? 0 : (self.state.slideWidth * self.state.currentSlide + (self.touchObject.length * self.touchObject.direction)) * -1,
            top: self.props.vertical ? (self.state.slideWidth * self.state.currentSlide + (self.touchObject.length * self.touchObject.direction)) * -1 : 0
          });
        },
        onTouchEnd: function(e) {
          self.handleSwipe(e);
        },
        onTouchCancel: function(e) {
          self.handleSwipe(e);
        }
      }
    },

    clickSafe: true,

    getMouseEvents: function () {
      var self = this;

      if (this.props.dragging === false) {
        return null;
      }

      return {
        onMouseDown: function (e) {
          self.touchObject = {
              startX: e.clientX,
              startY: e.clientY
          };

          self.setState({
            dragging: true
          });
        },
        onMouseMove: function(e) {
          if (!self.state.dragging) {
            return;
          }

          var direction = self.swipeDirection(
            self.touchObject.startX,
            e.clientX,
            self.touchObject.startY,
            e.clientY
          );

          if (direction !== 0) {
            e.preventDefault();
          }

          var length = self.props.vertical ? Math.round(Math.sqrt(Math.pow(e.clientY - self.touchObject.startY, 2)))
                                           : Math.round(Math.sqrt(Math.pow(e.clientX - self.touchObject.startX, 2)))

          self.touchObject = {
            startX: self.touchObject.startX,
            startY: self.touchObject.startY,
            endX: e.clientX,
            endY: e.clientY,
            length: length,
            direction: direction
          };

          self.setState({
            left: self.props.vertical ? 0 : self.getTargetLeft(self.touchObject.length * self.touchObject.direction),
            top: self.props.vertical ? self.getTargetLeft(self.touchObject.length * self.touchObject.direction) : 0
          });
        },
        onMouseUp: function(e) {
          if (!self.state.dragging) {
            return;
          }

          self.handleSwipe(e);
        },
        onMouseLeave: function(e) {
          if (!self.state.dragging) {
            return;
          }

          self.handleSwipe(e);
        }
      }
    },

    handleClick: function(e) {
      if (this.clickSafe === true) {
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopPropagation();
      }
    },

    handleSwipe: function(e) {
      if (typeof (this.touchObject.length) !== 'undefined' && this.touchObject.length > 44) {
        this.clickSafe = true;
      } else {
        this.clickSafe = false;
      }

      if (this.touchObject.length > (this.state.slideWidth / this.props.slidesToShow) / 5) {
        if (this.touchObject.direction === 1) {
          if (this.state.currentSlide >= this.props.children.length - this.props.slidesToScroll) {
            this.animateSlide(makeGlobal.tweenState.easingTypes[this.props.edgeEasing]);
          } else {
            this.nextSlide();
          }
        } else if (this.touchObject.direction === -1) {
          if (this.state.currentSlide <= 0) {
            this.animateSlide(makeGlobal.tweenState.easingTypes[this.props.edgeEasing]);
          } else {
            this.previousSlide();
          }
        }
      } else {
        this.goToSlide(this.state.currentSlide);
      }

      this.touchObject = {};

      this.setState({
        dragging: false
      });
    },

    swipeDirection: function(x1, x2, y1, y2) {

      var xDist, yDist, r, swipeAngle;

      xDist = x1 - x2;
      yDist = y1 - y2;
      r = Math.atan2(yDist, xDist);

      swipeAngle = Math.round(r * 180 / Math.PI);
      if (swipeAngle < 0) {
        swipeAngle = 360 - Math.abs(swipeAngle);
      }
      if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
        return 1;
      }
      if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
        return 1;
      }
      if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
        return -1;
      }
      if (this.props.vertical === true) {
        if ((swipeAngle >= 35) && (swipeAngle <= 135)) {
          return 1;
        } else {
          return -1;
        }
      }
      return 0;

    },

    // Action Methods

    goToSlide: function(index) {
      var self = this;
      if (index >= this.props.children.length || index < 0) {
        return;
      }
      this.setState({
        currentSlide: index
      }, function () {
        self.animateSlide();
        self.setExternalData();
      });
    },

    nextSlide: function () {
      var self = this;
      if ((this.state.currentSlide + this.props.slidesToScroll) >= this.props.children.length) {
        return;
      }
      this.setState({
        currentSlide: this.state.currentSlide + this.props.slidesToScroll
      }, function () {
        self.animateSlide();
        self.setExternalData();
      });
    },

    previousSlide: function () {
      var self = this;
      if ((this.state.currentSlide - this.props.slidesToScroll) < 0) {
        return;
      }
      this.setState({
        currentSlide: this.state.currentSlide - this.props.slidesToScroll
      }, function () {
        self.animateSlide();
        self.setExternalData();
      });
    },

    // Animation

    animateSlide: function(easing, duration, endValue) {
      this.tweenState(this.props.vertical ? 'top' : 'left', {
        easing: easing || makeGlobal.tweenState.easingTypes[this.props.easing],
        duration: duration || this.props.speed,
        endValue: endValue || this.getTargetLeft()
      });
    },

    getTargetLeft: function(touchOffset) {
      var offset;
      switch (this.props.cellAlign) {
        case 'left': {
          offset = 0;
          offset -= this.props.cellSpacing * (this.state.currentSlide);
          break;
        }
        case 'center': {
          offset = (this.state.frameWidth - this.state.slideWidth) / 2;
          offset -= this.props.cellSpacing * (this.state.currentSlide);
          break;
        }
        case 'right': {
          offset = this.state.frameWidth - this.state.slideWidth;
          offset -= this.props.cellSpacing * (this.state.currentSlide);
          break;
        }
      }

      if (this.props.vertical) {
        offset = offset / 2;
      }

      offset -= touchOffset || 0;

      return ((this.state.slideWidth * this.state.currentSlide) - offset) * -1;
    },

    // Bootstrapping

    bindEvents: function () {
      var self = this;
      if (ExecutionEnvironment.canUseDOM) {
        addEvent(window, 'resize', self.onResize);
        addEvent(document, 'readystatechange', self.onReadyStateChange);
      }
    },

    onResize: function () {
      this.setDimensions();
    },

    onReadyStateChange: function(event) {
      this.setDimensions();
    },

    unbindEvents: function () {
      var self = this;
      if (ExecutionEnvironment.canUseDOM) {
        removeEvent(window, 'resize', self.onResize);
        removeEvent(document, 'readystatechange', self.onReadyStateChange);
      }
    },

    formatChildren: function(children) {
      var self = this;
      return children.map(function(child, index) {
        return <li className="slider-slide" style={self.getSlideStyles()} key={index}>{child}</li>
      });
    },

    setInitialDimensions: function () {
      var self = this, slideWidth, frameWidth, frameHeight, slideHeight;

      slideWidth = this.props.vertical ? (this.props.initialSlideHeight || 0) : (this.props.initialSlideWidth || 0);
      slideHeight = this.props.initialSlideHeight ? this.props.initialSlideHeight * this.props.slidesToShow : 0;

      frameHeight = slideHeight + ((this.props.cellSpacing / 2) * (this.props.slidesToShow - 1));

      this.setState({
        frameWidth: this.props.vertical ? frameHeight : "100%",
        slideCount: this.props.children.length,
        slideWidth: slideWidth
      }, function () {
        self.setLeft();
        self.setExternalData();
      });
    },

    setDimensions: function () {
      var self = this, slideWidth, firstSlide, frame, frameHeight, slideHeight;

      frame = React.findDOMNode(this.refs.frame);
      firstSlide = frame.childNodes[0].childNodes[0];
      firstSlide.style.height = 'auto';
      slideHeight = firstSlide.offsetHeight * this.props.slidesToShow;

      if (typeof this.props.slideWidth !== 'number') {
        slideWidth = parseInt(this.props.slideWidth);
      } else {
        if (this.props.vertical) {
          slideWidth = (slideHeight / this.props.slidesToShow) * this.props.slideWidth;
        } else {
          slideWidth = (frame.offsetWidth / this.props.slidesToShow) * this.props.slideWidth;
        }
      }

      if (!this.props.vertical) {
        slideWidth -= this.props.cellSpacing * ((100 - (100 / this.props.slidesToShow)) / 100);
      }

      frameHeight = slideHeight + ((this.props.cellSpacing / 2) * (this.props.slidesToShow - 1));

      this.setState({
        frameWidth: this.props.vertical ? frameHeight : frame.offsetWidth,
        slideCount: this.props.children.length,
        slideWidth: slideWidth
      }, function () {
        self.setLeft();
        self.setExternalData();
      });
    },

    setLeft: function () {
      this.setState({
        left: this.props.vertical ? 0 : this.getTargetLeft(),
        top: this.props.vertical ? this.getTargetLeft() : 0
      })
    },

    // Data

    setExternalData: function () {
      if (this.props.data) {
        this.props.data();
      }
    },

    // Styles

    getListStyles: function () {
      var listWidth = this.state.slideWidth * this.props.children.length;
      var spacingOffset = this.props.cellSpacing * this.props.children.length;
      return {
        position: 'relative',
        display: 'block',
        top: this.getTweeningValue('top'),
        left: this.getTweeningValue('left'),
        margin: this.props.vertical ? (this.props.cellSpacing / 2) * -1 + 'px 0px'
                                    : '0px ' + (this.props.cellSpacing / 2) * -1 + 'px',
        padding: 0,
        height: this.props.vertical ? listWidth + spacingOffset : 'auto',
        width: this.props.vertical ? 'auto' : listWidth + spacingOffset,
        cursor: this.state.dragging === true ? 'pointer' : 'inherit',
        transform: 'translate3d(0, 0, 0)',
        WebkitTransform: 'translate3d(0, 0, 0)',
        boxSizing: 'border-box',
        MozBoxSizing: 'border-box'
      }
    },

    getFrameStyles: function () {
      return {
        position: 'relative',
        display: 'block',
        overflow: 'hidden',
        height: this.props.vertical ? this.state.frameWidth || 'initial' : 'auto',
        margin: this.props.framePadding,
        padding: 0,
        transform: 'translate3d(0, 0, 0)',
        WebkitTransform: 'translate3d(0, 0, 0)',
        boxSizing: 'border-box',
        MozBoxSizing: 'border-box'
      }
    },

    getSlideStyles: function () {
      return {
        display: this.props.vertical ? 'block' : 'inline-block',
        listStyleType: 'none',
        verticalAlign: 'middle', // used to be top
        width: this.props.vertical ? '100%' : this.state.slideWidth,
        height: 'auto',
        boxSizing: 'border-box',
        MozBoxSizing: 'border-box',
        marginLeft: this.props.vertical ? 'auto' : this.props.cellSpacing / 2,
        marginRight: this.props.vertical ? 'auto' : this.props.cellSpacing / 2,
        marginTop: this.props.vertical ? this.props.cellSpacing / 2 : 'auto',
        marginBottom: this.props.vertical ? this.props.cellSpacing / 2 : 'auto'
      }
    },

    getSliderStyles: function () {
      return {
        position: 'relative',
        display: 'block',
        width: this.props.width,
        height: 'auto',
        boxSizing: 'border-box',
        MozBoxSizing: 'border-box',
        visibility: this.state.slideWidth ? 'visible' : 'hidden',

        userSelect: 'none',
        WebkitUserSelect: 'none',
        WebkitTouchCallout: 'none',
        MozUserSelect: 'none',
        msUserSelect: 'none',
      }
    },

    getStyleTagStyles: function () {
      return '.slider-slide > img {width: 100%; display: block;}'
    },

    getDecoratorStyles: function(position) {
      switch (position) {
        case 'TopLeft': {
          return {
            position: 'absolute',
            top: 0,
            left: 0
          }
        }
        case 'TopCenter': {
          return {
            position: 'absolute',
            top: 0,
            left: '50%',
            transform: 'translateX(-50%)',
            WebkitTransform: 'translateX(-50%)'
          }
        }
        case 'TopRight': {
          return {
            position: 'absolute',
            top: 0,
            right: 0
          }
        }
        case 'CenterLeft': {
          return {
            position: 'absolute',
            top: '50%',
            left: 0,
            transform: 'translateY(-50%)',
            WebkitTransform: 'translateY(-50%)'
          }
        }
        case 'CenterCenter': {
          return {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%,-50%)',
            WebkitTransform: 'translate(-50%, -50%)'
          }
        }
        case 'CenterRight': {
          return {
            position: 'absolute',
            top: '50%',
            right: 0,
            transform: 'translateY(-50%)',
            WebkitTransform: 'translateY(-50%)'
          }
        }
        case 'BottomLeft': {
          return {
            position: 'absolute',
            bottom: 0,
            left: 0
          }
        }
        case 'BottomCenter': {
          return {
            position: 'absolute',
            bottom: 0,
            left: '50%',
            transform: 'translateX(-50%)',
            WebkitTransform: 'translateX(-50%)'
          }
        }
        case 'BottomRight': {
          return {
            position: 'absolute',
            bottom: 0,
            right: 0
          }
        }
        default: {
          return {
            position: 'absolute',
            top: 0,
            left: 0
          }
        }
      }
    }

  });

  makeGlobal.Carousel.ControllerMixin = {
    getInitialState: function () {
      return {
        carousels: {}
      }
    },
    setCarouselData: function(carousel) {
      var data = this.state.carousels;
      data[carousel] = this.refs[carousel];
      this.setState({
        carousels: data
      });
    }
  }

});
