Meteor.startup(function () { // we defer until startup so that HCConfig is defined

  jQuery(window).ready(function() {

    setTimeout(function () { // desperate hack to try and fix this

      window.fbAsyncInit = function() {
        FB.init({
          appId      : HCConfig.facebookAppId,
          xfbml      : false, // we do not want to parse the page for xfbml tags
          version    : 'v2.3'
        });

        jQuery('#fb-root').trigger('facebook:init');
      };

      (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));

    });

  }, 1000);

});

/** Load Facebook Tag API **/
// can be used for Custom Audience & Conversion Tracking
// see https://developers.facebook.com/docs/ads-for-websites/tag-api/

initFacebookTagApi = function () {}
//initFacebookTagApi = function () {

  // this code is taken from https://www.facebook.com/ads/manager/pixel_detail/detail/?act=23889604&pixel_id=1605121283071025
  // ie the facebook ads system for embedding a pixel

  // (function() {
  // var _fbq = window._fbq || (window._fbq = []);
  // if (!_fbq.loaded) {
  // var fbds = document.createElement('script');
  // fbds.async = true;
  // fbds.src = '//connect.facebook.net/en_US/fbds.js';
  // var s = document.getElementsByTagName('script')[0];
  // s.parentNode.insertBefore(fbds, s);
  // _fbq.loaded = true;
  // }
  // _fbq.push(['addPixelId', '1605121283071025']);
  // })();
  // window._fbq = window._fbq || [];
  // window._fbq.push(['track', 'PixelInitialized', {}]); 

  // this code came from https://developers.facebook.com/docs/ads-for-websites/tag-api/
  // does not seem to work anymore
  // WTF: doing it in Meteor.startup seems to fix the issue... it's a bit of a mystery why.... but it's working.... (must be something to do with scoping...)

  Meteor.startup(function () {

    // console.log("load tag api");
    // console.log(window);
    // console.log(document);

    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');
    
    // Insert Your Custom Audience Pixel ID below. 
    fbq('init', '1605121283071025');
    fbq('track', 'PageView');

  });

//}

/* 

deferUntilFBInit:
Require FB to be loaded into the window before this is called

*/

deferUntilFBInit = function (_callback) {
  if (window.FB) {
    _callback();
  } else {
    jQuery('#fb-root').one('facebook:init', _callback);
  }
}