deferUntilLinkedInInit = function (_callback) {
	if (window.IN) {
		_callback();
	} else {
		$.getScript("https://platform.linkedin.com/in.js?async=true", function success() {
			IN.init();
			_callback();
		});
	}
}