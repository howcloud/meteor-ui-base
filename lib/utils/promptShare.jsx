/**
 * 
 */

/** 

Prompt Share
Shows a modal prompt for the user to share a certain url on social media
They can dismiss it by clicking outside (as it is a modal) or clicking the done button

options = {
    title: title of the prompt
    url: url of what we want to share
    description: description of the prompt

    utm_campaign
    utm_content

    render: optional render function to render before we show the social share buttons (ie to illustrate what the user is going to be sharing)
            if used, displays after the description

    facebook: whether we want to show facebook share (default true)
    twitter: whether we want to show twitter tweet (default true)
    linkedin: whether we want to show linkedin share (default false)
}

**/

promptShare = function (options) {
    options = options || {};

    /* Props */

    options.props = options.props || {}
    var _props = {title: 'Share'} // default props
    _.extend(_props, options.props);
    delete options.props;

    /* Render Handler */

    var _render = options.render;
    delete options.render;

    /* Generate launchModal */

    return launchModal(_.extend({
        props: _props,

        render: function () {
            var url = makeUrl(this.props.url, {
                utm_campaign: this.props.utm_campaign,
                utm_content: this.props.utm_content
            });

            return (
                <div>
                    <div className="content-box-pad">
                        {options.description ? <p className="strong">{options.description}</p> : null}
                        {_render ? <div className="gapBottom">{_render.call(this)}</div> : null}
                        <Social_Boxes url={url} facebook={this.props.facebook} twitter={this.props.twitter} linkedin={this.props.linkedin} />
                    </div>

                    <div className="content-box-section content-box-shade content-box-pad inline-children text-right">
                        <Button onClick={this.done_click}><FontAwesome icon="thumbs-up" /> Done!</Button>
                    </div>
                </div>
            );
        },

        done_click: function () {
            this.cancel();
            if (this.onDone) this.onDone();
        },
    }, options));

}
