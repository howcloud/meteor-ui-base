/**
 * 
 */

/** 

launchModal
Launches a hollow modal which we can render inside of and delegates the rendering

options = {
    title: title of the prompt
    onCancel: allows override of cancel callback
}

**/

launchModal = function (options) {
    options = options || {};

    _render = options.render || function () { return null; }
    delete options.render;

    return delegateRender(_.extend({
        render: function () {
            return (
                <ModalContentBox onCancel={this.cancel} title={this.props.title} _delegateRender={false} style={this.props.style}>
                    {_render.call(this)}
                </ModalContentBox>
            );
        }
    }, options));

}
